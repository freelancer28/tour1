
<?php 

function checkuser ($conn,$data,&$erro){
	$check =$conn->prepare("SELECT * FROM ".DB_USERS." WHERE username=:username and password=:password");
		$check->bindParam(":username",$data['user'],PDO::PARAM_STR);
		$check->bindParam(":password",$data['pass'],PDO::PARAM_STR);
		$check->execute();
		$count = $check->rowCount();
		if ($count ==1 ) {
			$data1 = $check->fetch(PDO::FETCH_ASSOC);
			$_SESSION["dh_user"]=$data1['username'];
			$_SESSION["dh_level"]= $data1['level'];
			header("location:index.php");
			
		}else{
			$erro = "Tài khoản tên đăng nhập sai!";
		}
}

//USER --------------------
	//1.ADD-USER
	function add_user($conn,$data,&$erro){
		$check =$conn->prepare("SELECT * FROM ".DB_USERS." WHERE username=:username");
		$check->bindParam(":username",$data['name'],PDO::PARAM_STR);
		$check->execute();
		$count = $check->rowCount();
		if($count != 0){
			$erro="Dữ liệu đã bị trùng!";
		}else{
			$stmt=$conn->prepare("INSERT INTO ".DB_USERS." (username,password,email,sdt,level)VALUES (:username,:password,:email,:sdt,:level)");
			$stmt->bindParam(":username",$data["name"],PDO::PARAM_STR);
			$stmt->bindParam(":password",$data["password"],PDO::PARAM_STR);
			$stmt->bindParam(":email",$data["email"],PDO::PARAM_STR);
			$stmt->bindParam(":sdt",$data["sdt"],PDO::PARAM_STR);
			$stmt->bindParam(":level",$data["level"],PDO::PARAM_INT);
			$stmt->execute();
			chuyentrang('list-user');
		}
	}
	//2.LIST-USER
	function list_user($conn){
		$stmt= $conn->prepare("SELECT * FROM ".DB_USERS);
		$stmt->execute();
		$data=$stmt->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}
//END---USER0---------------

// LOẠI TOUR -------------------------------

	// 1. THÊM LOẠI TOUR
	function add_loai_tour($conn,$data,&$erro){
		$check = $conn->prepare("SELECT name FROM ".DBLOAITOUR." WHERE name=:name");
		$check->bindParam(":name",$data['name'],PDO::PARAM_STR);
		$check->execute();
		$count=$check->rowCount();
		if ($count == 0) {
			$stmt=$conn->prepare("INSERT  INTO ".DBLOAITOUR." (name,mo_ta) VALUES (:name,:mo_ta)");
			$stmt->bindParam(":name",$data['name'],PDO::PARAM_STR);
			$stmt->bindParam(":mo_ta",$data['mo_ta'],PDO::PARAM_STR);
			$stmt->execute();
			chuyentrang('list-loai-tour');
		}else{
			$erro= "Có dữ liệu rồi vui lòng nhập lại!";
			
		}
	}


	// 2.lIST LOẠI TOUR
	function list_loai_tour($conn){
		$stmt=$conn->prepare('SELECT * FROM '.DBLOAITOUR.' ORDER BY id ASC');
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}
	//3. SỬA LOAI TOUR
		// 3.1 load loại tour cần sửa lên wiev
		function load_loai_tour($conn,$cid){
			$load= $conn->prepare("SELECT * FROM ".DBLOAITOUR." WHERE id=:cid");
			$load->bindParam(":cid",$cid,PDO::PARAM_INT);
			$load->execute();
			$data= $load->fetch(PDO::FETCH_ASSOC);
			return $data;
		}
		// 3.2 Cập nhật loại tour.
		function edit_loai_tour($conn,$data,&$erro){
			$check = $conn->prepare("SELECT name FROM ".DBLOAITOUR." WHERE name=:name");
			$check->bindParam(":name",$data['name'],PDO::PARAM_STR);
			$check->execute();
			$count=$check->rowCount();
			if ($count == 0) {
			$stmt=$conn->prepare("UPDATE ".DBLOAITOUR." SET name=:name,mo_ta=:mo_ta WHERE id=:id");
			$stmt->bindParam(":name",$data['name'],PDO::PARAM_STR);
			$stmt->bindParam(":mo_ta",$data['mo_ta'],PDO::PARAM_STR);
			$stmt->bindParam(":id",$data['id'],PDO::PARAM_INT);
			$stmt->execute();
			chuyentrang('list-loai-tour');
			}else{
				$check = $conn->prepare("SELECT mo_ta FROM ".DBLOAITOUR." WHERE id=:id");
				$check->bindParam(":id",$data['id'],PDO::PARAM_INT);
				$check->execute();
				$mo_ta = $check->fetch(PDO::FETCH_ASSOC);
				if ($mo_ta == $data['mo_ta']) {
					$erro= "Có dữ liệu rồi vui lòng nhập lại!";
				}else{
					$stmt=$conn->prepare("UPDATE ".DBLOAITOUR." SET mo_ta=:mo_ta WHERE id=:id");
					$stmt->bindParam(":mo_ta",$data['mo_ta'],PDO::PARAM_STR);
					$stmt->bindParam(":id",$data['id'],PDO::PARAM_INT);
					$stmt->execute();
					chuyentrang('list-loai-tour');
				}
			}
			
		}
	//4.XÓA DANH MỤC LOẠI TOUR
	function delete_loai_tour($conn,$id){
		$stmt= $conn->prepare("DELETE FROM ".DBLOAITOUR." WHERE id=:id");
		$stmt->bindParam(":id",$id,PDO::PARAM_INT);
		$stmt->execute();
		chuyentrang("list-loai-tour");
	}
	



// END -----------------LOẠI TOUR -------------------------------


// --------Tour-----------------------------------

	//1.THEM TOUR
		//1.1 load để thêm tour
		function load_cmb_loai_tour($conn){
			$stmt=$conn->prepare('SELECT * FROM '.DBLOAITOUR.' ORDER BY id ASC');
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		//1.2 add tour
		function add_tour($conn,$data,&$erro){
			$check = $conn->prepare("SELECT ten_tour FROM ".DBTOUR." WHERE ten_tour=:ten_tour");
			$check->bindParam(":ten_tour",$data['ten_tour'],PDO::PARAM_STR);
			$check->execute();
			$count=$check->rowCount();
			if ($count == 0) {

				$bonhotam =$data['tmp_name'];
				move_uploaded_file($bonhotam,"../temp/upload/img_tour_index/".$data['img']);
				$stmt=$conn->prepare("INSERT  INTO ".DBTOUR." (ten_tour,dia_diem,thoi_gian,tom_tat,an_hien,gia_tour,id_loai_tours,img) VALUES (:ten_tour,:dia_diem,:thoi_gian,:tom_tat,:an_hien,:gia_tour,:id_loai_tours,:img)");
				$stmt->bindParam(":ten_tour",$data['ten_tour'],PDO::PARAM_STR);
				$stmt->bindParam(":dia_diem",$data['dia_diem'],PDO::PARAM_STR);
				$stmt->bindParam(":thoi_gian",$data['thoi_gian'],PDO::PARAM_INT);
				$stmt->bindParam(":tom_tat",$data['tom_tat'],PDO::PARAM_STR);
				$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_STR);
				$stmt->bindParam(":gia_tour",$data['gia_tour'],PDO::PARAM_STR);
				$stmt->bindParam(":id_loai_tours",$data['id_loai'],PDO::PARAM_INT);
				$stmt->bindParam(":img",$data['img'],PDO::PARAM_STR);
				$stmt->execute();
				chuyentrang('list-tour');
			}else{
				$erro= "Có dữ liệu rồi vui lòng nhập lại!";
				
			}
		}

	// 2.lIST  TOUR
	function list_tour($conn){
		$stmt=$conn->prepare('SELECT * FROM '.DBTOUR.' ORDER BY id ASC');
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}

	function load_show_loaitour($conn,$id){
		$stmt=$conn->prepare('SELECT * FROM '.DBLOAITOUR.' WHERE id=:id');
		$stmt->bindParam(":id",$id,PDO::PARAM_INT);
		$stmt->execute();
		$data = $stmt->fetch(PDO::FETCH_ASSOC);
		return $data;
	}

	//3. SỬA TOUR
		// 3.1 load tour cần sửa lên wiev
		function load_tour($conn,$cid){
			$load= $conn->prepare("SELECT * FROM ".DBTOUR." WHERE id=:cid");
			$load->bindParam(":cid",$cid,PDO::PARAM_INT);
			$load->execute();
			$data= $load->fetch(PDO::FETCH_ASSOC);
			return $data;
		}
		// 3.2 Cập nhật tour.
		function edit_tour($conn,$data){
			unlink('../temp/upload/img_tour_index/'.$data['img_cu']);
			$stmt=$conn->prepare("UPDATE ".DBTOUR." SET ten_tour=:ten_tour,dia_diem=:dia_diem,thoi_gian=:thoi_gian,tom_tat=:tom_tat,an_hien=:an_hien,gia_tour=:gia_tour,id_loai_tours=:id_loai_tours,img=:img  WHERE id=:id");
			$stmt->bindParam(":ten_tour",$data['ten_tour'],PDO::PARAM_STR);
			$stmt->bindParam(":dia_diem",$data['dia_diem'],PDO::PARAM_STR);
			$stmt->bindParam(":thoi_gian",$data['thoi_gian'],PDO::PARAM_INT);
			$stmt->bindParam(":tom_tat",$data['tom_tat'],PDO::PARAM_STR);
			$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_STR);
			$stmt->bindParam(":gia_tour",$data['gia_tour'],PDO::PARAM_STR);
			$stmt->bindParam(":id_loai_tours",$data['id_loai'],PDO::PARAM_INT);
			$stmt->bindParam(":img",$data['img'],PDO::PARAM_STR);
			$stmt->bindParam(":id",$data['id'],PDO::PARAM_INT);
			$stmt->execute();
			$bonhotam =$data['tmp_name'];
				move_uploaded_file($bonhotam,"../temp/upload/img_tour_index/".$data['img']);
			chuyentrang('list-tour');
			
		}
	//4.XÓA DANH MỤC  TOUR
		function lay_ten_img($conn,$id){
			$load= $conn->prepare("SELECT * FROM ".DBTOUR." WHERE id=:cid");
			$load->bindParam(":cid",$id,PDO::PARAM_INT);
			$load->execute();
			$data= $load->fetch(PDO::FETCH_ASSOC);
			return $data;
		}

	function delete_tour($conn,$id){

		$data =lay_ten_img($conn,$id);
		$stmt= $conn->prepare("DELETE FROM ".DBTOUR." WHERE id=:id");
		$stmt->bindParam(":id",$id,PDO::PARAM_INT);
		$stmt->execute();
		unlink('../temp/upload/img_tour_index/'.$data['img']);
		chuyentrang("list-tour");

	}
	function delete_lich_trinh($conn,$id){

		$data =lay_ten_img($conn,$id);
		$stmt= $conn->prepare("DELETE FROM ".DB_LICHTRINH." WHERE id=:id");
		$stmt->bindParam(":id",$id,PDO::PARAM_INT);
		$stmt->execute();
		unlink('../temp/upload/lichtrinh/'.$data['img']);
		chuyentrang1("list-lich-trinh");

	}
			

// end 	-----TOUR -------------------------------

// CHI TIẾT TOUR-----------------------------------
	//1.THEM chi tiết tour
		//1.1 load để thêm chi tiết tour
		function load_cmb_tour($conn){
			$stmt=$conn->prepare('SELECT * FROM '.DBTOUR.' ORDER BY id ASC');
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		// 1.2 THÊM chi tiet TOUR
		function add_chi_tiet_tour($conn,$data,&$erro){
			$check = $conn->prepare("SELECT * FROM ".DBCT_TOUR." WHERE id_tour=:id_tour");
			$check->bindParam(":id_tour",$data['id_tour'],PDO::PARAM_STR);
			$check->execute();
			$count=$check->rowCount();
			if ($count == 0) {
				$stmt=$conn->prepare("INSERT  INTO ".DBCT_TOUR." (noi_dung,an_hien,id_tour) VALUES (:noi_dung,:an_hien,:id_tour)");
				$stmt->bindParam(":noi_dung",$data['noi_dung'],PDO::PARAM_STR);
				$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_INT);
				$stmt->bindParam(":id_tour",$data['id_tour'],PDO::PARAM_INT);
				$stmt->execute();
				chuyentrang('list-ct-tour');
			}else{
				$erro="Dữ liệu đã tồn tại !";
			}
			
		}

		//1.3 addn bao gom 
		function add_bao_gom($conn,$data){
				$stmt=$conn->prepare("INSERT  INTO ".DB_BAOGOM." (noi_dung,id_chi_tiet_tour) VALUES (:noi_dung,:id_chi_tiet_tour)");
				$stmt->bindParam(":noi_dung",$data['noi_dung'],PDO::PARAM_STR);
				$stmt->bindParam(":id_chi_tiet_tour",$data['id_tour'],PDO::PARAM_INT);
				$stmt->execute();
				chuyentrang('list-bao-gom-ct-tour');
		}
		function add_k_bao_gom($conn,$data){
				$stmt=$conn->prepare("INSERT  INTO ".DB_K_BAOGOM." (noi_dung,id_chi_tiet_tour) VALUES (:noi_dung,:id_chi_tiet_tour)");
				$stmt->bindParam(":noi_dung",$data['noi_dung'],PDO::PARAM_STR);
				$stmt->bindParam(":id_chi_tiet_tour",$data['id_tour'],PDO::PARAM_INT);
				$stmt->execute();
				chuyentrang('list-k-bao-gom-ct-tour');
		}
		function add_ha_cttour($conn,$data){
			foreach($data['img_name'] as $key=>$value)
		        {
		            
		            $stmt=$conn->prepare("INSERT  INTO ".DB_IMGCTTOUR." (img,an_hien,id_chi_tiet_tour) VALUES (:img,:an_hien,:id_chi_tiet_tour)");
					$stmt->bindParam(":img",$data['img_name'][$key],PDO::PARAM_STR);
					$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_INT);
					$stmt->bindParam(":id_chi_tiet_tour",$data['id_tour'],PDO::PARAM_INT);
					$stmt->execute();
					move_uploaded_file($data['img_tmp'][$key],"../temp/upload/img_ct_tour/".$data['img_name'][$key]);
		        }
		        chuyentrang1('list-img-ct-tour');
				//chuyentrang('list-tour');
		}

	// 2.lIST  TOUR
		function load_show_tour_x($conn,$id){
			$stmt=$conn->prepare('SELECT * FROM '.DBTOUR.' WHERE id=:id');
			$stmt->bindParam(":id",$id,PDO::PARAM_INT);
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			return $data;
		}

		function list_ct_tour($conn){
			$stmt=$conn->prepare('SELECT * FROM '.DBCT_TOUR.' ORDER BY id ASC');
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}

		function list_bg_ct_tour($conn){
			$stmt=$conn->prepare('SELECT * FROM '.DB_BAOGOM.' ORDER BY id ASC');
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		function list_k_bg_ct_tour($conn){
			$stmt=$conn->prepare('SELECT * FROM '.DB_K_BAOGOM.' ORDER BY id ASC');
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		function list_img_ct_tour($conn){
			$stmt=$conn->prepare('SELECT * FROM '.DB_IMGCTTOUR.' ORDER BY id ASC');
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
	//3. Sửa ct tour
		//3.1 load để sửa tour:
		function load_ct_tour_01($conn,$id){
			$stmt=$conn->prepare('SELECT * FROM '.DBCT_TOUR.' WHERE id=:id');
			$stmt->bindParam(":id",$id,PDO::PARAM_INT);
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			return $data;
		}
		//3.2 sua tour
		function edit_ct_tour($conn,$data){
			$stmt=$conn->prepare("UPDATE ".DBCT_TOUR." SET noi_dung=:noi_dung,an_hien=:an_hien,id_tour=:id_tour WHERE id=:id");
			$stmt->bindParam(":noi_dung",$data['noi_dung'],PDO::PARAM_STR);
			$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_INT);
			$stmt->bindParam(":id_tour",$data['id_tour'],PDO::PARAM_INT);
			$stmt->bindParam(":id",$data['cid'],PDO::PARAM_INT);
			$stmt->execute();
			chuyentrang('list-ct-tour');
		}
	//4. delete tour
		function delete_ct_tour($conn,$cid){
			$stmt= $conn->prepare("DELETE FROM ".DBCT_TOUR." WHERE id=:id");
			$stmt->bindParam(":id",$cid,PDO::PARAM_INT);
			$stmt->execute();
			chuyentrang("list-ct-tour");
		}
		function delete_bg_ct_tour($conn,$cid){
			$stmt= $conn->prepare("DELETE FROM ".DB_BAOGOM." WHERE id=:id");
			$stmt->bindParam(":id",$cid,PDO::PARAM_INT);
			$stmt->execute();
			chuyentrang("list-bao-gom-ct-tour");
		}
		function delete_k_bg_ct_tour($conn,$cid){
			$stmt= $conn->prepare("DELETE FROM ".DB_K_BAOGOM." WHERE id=:id");
			$stmt->bindParam(":id",$cid,PDO::PARAM_INT);
			$stmt->execute();
			chuyentrang("list-k-bao-gom-ct-tour");
		}
		function lay_ten_img21($conn,$id){
			$load= $conn->prepare("SELECT * FROM ".DB_IMGCTTOUR." WHERE id=:cid");
			$load->bindParam(":cid",$id,PDO::PARAM_INT);
			$load->execute();
			$data= $load->fetch(PDO::FETCH_ASSOC);
			return $data;
		}
		function delete_img_ct_tour($conn,$id){
			$data =lay_ten_img21($conn,$id);
			$stmt= $conn->prepare("DELETE FROM ".DB_IMGCTTOUR." WHERE id=:id");
			$stmt->bindParam(":id",$id,PDO::PARAM_INT);
			$stmt->execute();
			unlink('../temp/upload/img_ct_tour/'.$data['img']);
			chuyentrang("list-img-ct-tour");
		}

		function add_lich_trinh($conn,$data,&$erro){
			$check = $conn->prepare(" SELECT * FROM gtm_lich_trinh_tour WHERE ngay=:ngay and id_ct_tour=:id_ct_tour");
			$check->bindParam(":ngay",$data['id_tour'],PDO::PARAM_INT);
			$check->bindParam(":id_ct_tour",$data['thoi_gian'],PDO::PARAM_INT);
			$check->execute();
			$count= $check->rowCount();
			if ($count == 0 ) {
				$stmt= $conn->prepare("INSERT INTO gtm_lich_trinh_tour (tieu_de,noi_dung,ngay,img,id_ct_tour) VALUES (:tieu_de,:noi_dung,:ngay,:img,:id_ct_tour)");
				$stmt->bindParam(":tieu_de",$data['tieu_de'],PDO::PARAM_STR);
				$stmt->bindParam(":noi_dung",$data['noi_dung'],PDO::PARAM_STR);
				$stmt->bindParam(":ngay",$data['id_tour'],PDO::PARAM_INT);
				$stmt->bindParam(":img",$data['img'],PDO::PARAM_STR);
				$stmt->bindParam(":id_ct_tour",$data['id_tour'],PDO::PARAM_INT);
				$stmt->execute();
				move_uploaded_file($data['tmp_name'],"../temp/upload/lichtrinh/".$data['img']);
				chuyentrang1('list-lich-trinh');
			}else{
				$erro= "Dữ liệu đã tồn tại vui lòng chọn lại";
			}
		}

// END - CHI TIET TOUR-----------------------------
	

// --------SLIDER-----------------------------------
	function load_cttour($conn){
		$stmt=$conn->prepare('SELECT * FROM '.DBTOUR.' ORDER BY id ASC');
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
	}
	function value_ctour($conn,$id_tour){
			$stmt=$conn->prepare('SELECT * FROM '.DBCT_TOUR.' WHERE id_tour=:id_tour');
			$stmt->bindParam(":id_tour",$id_tour,PDO::PARAM_INT);
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			return $data;
	}

	//1.ADD slider 
	function add_slider($conn,$data){
			foreach($data['img_name'] as $key=>$value)
		        {
		            
		            $stmt=$conn->prepare("INSERT  INTO ".DB_SLIDER." (img,an_hien,id_chi_tiet_tour) VALUES (:img,:an_hien,:id_chi_tiet_tour)");
					$stmt->bindParam(":img",$data['img_name'][$key],PDO::PARAM_STR);
					$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_INT);
					$stmt->bindParam(":id_chi_tiet_tour",$data['id_tour'],PDO::PARAM_INT);
					$stmt->execute();
					move_uploaded_file($data['img_tmp'][$key],"../temp/upload/slider/".$data['img_name'][$key]);
		        }
		chuyentrang('list-slider');
	}
	//2.Load slider
	function load_slider($conn){
			$load= $conn->prepare('SELECT * FROM '.DB_SLIDER.' ORDER BY id ASC');
			$load->execute();
			$data= $load->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}

	function lay_thong_tin_tour($conn,$id_ct_tour){
		$stmt=$conn->prepare('SELECT * FROM '.DBCT_TOUR.' WHERE id=:id');
		$stmt->bindParam(":id",$id_ct_tour,PDO::PARAM_INT);
		$stmt->execute();
		$data = $stmt->fetch(PDO::FETCH_ASSOC);
		$id_tour= $data['id_tour'];
		$stmt1=$conn->prepare('SELECT * FROM '.DBTOUR.' WHERE id=:id_tour');
		$stmt1->bindParam(":id_tour",$id_tour,PDO::PARAM_INT);
		$stmt1->execute();
		$data1 = $stmt1->fetch(PDO::FETCH_ASSOC);
		return $data1;

	}
	//3. delete slider
	function lay_ten_img1($conn,$id){
			$load= $conn->prepare("SELECT * FROM ".DB_SLIDER." WHERE id=:cid");
			$load->bindParam(":cid",$id,PDO::PARAM_INT);
			$load->execute();
			$data= $load->fetch(PDO::FETCH_ASSOC);
			return $data;
		}
	function delete_slider($conn,$id){
		$data =lay_ten_img1($conn,$id);
		$stmt= $conn->prepare("DELETE FROM ".DB_SLIDER." WHERE id=:id");
		$stmt->bindParam(":id",$id,PDO::PARAM_INT);
		$stmt->execute();
		unlink('../temp/upload/thuvien/'.$data['img']);
		chuyentrang("list-slider");
	}


// END SLIDER--------------------------------

///THU VIỆN--------------------------------------

	//1. Thêm thư viện:
		function add_thu_vien($conn,$data,&$erro){
			$check = $conn->prepare("SELECT * FROM ".DB_THUVIEN." WHERE dia_diem=:dia_diem");
			$check->bindParam(":dia_diem",$data['ten_tvien'],PDO::PARAM_STR);
			$check->execute();
			$count=$check->rowCount();
			if ($count == 0) {
				$stmt=$conn->prepare("INSERT  INTO ".DB_THUVIEN." (dia_diem,an_hien) VALUES (:dia_diem,:an_hien)");
				$stmt->bindParam(":dia_diem",$data['ten_tvien'],PDO::PARAM_STR);
				$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_INT);
				$stmt->execute();
				//exit();
			}else{
				$erro= "Có dữ liệu rồi vui lòng nhập lại!";
				
			}
		}

	//3.ADD img thu vien 
	function add_img_thuvien($conn,$data){
			foreach($data['img_name'] as $key=>$value)
		        {
		            
		            $stmt=$conn->prepare("INSERT  INTO ".DB_IMGTHUVIEN." (img,an_hien,id_thu_vien) VALUES (:img,:an_hien,:id_thu_vien)");
					$stmt->bindParam(":img",$data['img_name'][$key],PDO::PARAM_STR);
					$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_INT);
					$stmt->bindParam(":id_thu_vien",$data['id_thu_vien'],PDO::PARAM_INT);
					$stmt->execute();
					move_uploaded_file($data['img_tmp'][$key],"../temp/upload/thuvien/".$data['img_name'][$key]);
		        }
		chuyentrang('list-thu-vien');
	}
	//2.them chi tiết thư viện
		function load_thuvien($conn){
			$load= $conn->prepare('SELECT * FROM '.DB_THUVIEN.' ORDER BY id ASC');
			$load->execute();
			$data= $load->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
	//3.Load thư viên ảnh

		function load_img_thuvien($conn){
			$load= $conn->prepare('SELECT * FROM '.DB_IMGTHUVIEN.' ORDER BY id ASC');
			$load->execute();
			$data= $load->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		//4. load list loại thư vien
		function load_list_loai_thuvien($conn,$id){
			$stmt=$conn->prepare('SELECT * FROM '.DB_IMGTHUVIEN.' WHERE id=:id');
			$stmt->bindParam(":id",$id,PDO::PARAM_INT);
			$stmt->execute();
			$data= $stmt->fetch(PDO::FETCH_ASSOC);
			return $data;
		}
		//5. delete thu vien
		function lay_ten_img2($conn,$id){
			$load= $conn->prepare("SELECT * FROM ".DB_IMGTHUVIEN." WHERE id=:cid");
			$load->bindParam(":cid",$id,PDO::PARAM_INT);
			$load->execute();
			$data= $load->fetch(PDO::FETCH_ASSOC);
			return $data;
		}
		function delete_thu_vien($conn,$id){
			$data =lay_ten_img2($conn,$id);
			$stmt= $conn->prepare("DELETE FROM ".DB_IMGTHUVIEN." WHERE id=:id");
			$stmt->bindParam(":id",$id,PDO::PARAM_INT);
			$stmt->execute();
			unlink('../temp/upload/thuvien/'.$data['img']);
			chuyentrang("list-thu-vien");
		}

//END -THƯ VIỆN-------------------------


//DỊCH VỤ --------------------------------
		//1. add dịch vụ
		function add_dv($conn,$data,&$erro){
			$check = $conn->prepare("SELECT ten_dv FROM ".DB_DICHVU." WHERE ten_dv=:ten_dv");
			$check->bindParam(":ten_dv",$data['ten_dich_vu'],PDO::PARAM_STR);
			$check->execute();
			$count=$check->rowCount();
			if ($count == 0) {

				$bonhotam =$data['tmp_name'];
				move_uploaded_file($bonhotam,"../temp/upload/dichvu/".$data['img']);
				$stmt=$conn->prepare("INSERT  INTO ".DB_DICHVU." (ten_dv,tom_tat,gia_dv,an_hien,img,tieu_de) VALUES (:ten_dv,:tom_tat,:gia_dv,:an_hien,:img,:tieu_de)");
				$stmt->bindParam(":ten_dv",$data['ten_dich_vu'],PDO::PARAM_STR);
				$stmt->bindParam(":tom_tat",$data['tom_tat'],PDO::PARAM_STR);
				$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_STR);
				$stmt->bindParam(":gia_dv",$data['gia_dv'],PDO::PARAM_STR);
				$stmt->bindParam(":img",$data['img'],PDO::PARAM_STR);
				$stmt->bindParam(":tieu_de",$data['tieu_de'],PDO::PARAM_STR);
				$stmt->execute();
				chuyentrang('list-dich-vu');
			}else{
				$erro= "Có dữ liệu rồi vui lòng nhập lại!";
				
			}
				}

	//2. list dich vụ
	function list_dich_vu($conn){
			$load= $conn->prepare('SELECT * FROM '.DB_DICHVU.' ORDER BY id ASC');
			$load->execute();
			$data= $load->fetchAll(PDO::FETCH_ASSOC);
			return $data;
	}
	//3.Load dịch vụ
	function load_dich_vu($conn,$id){
			$load= $conn->prepare('SELECT * FROM '.DB_DICHVU.' WHERE id=:id');
			$load->bindParam(":id",$id,PDO::PARAM_INT);
			$load->execute();
			$data= $load->fetch(PDO::FETCH_ASSOC);
			return $data;
	}
	//4. sua dich vu
	function sua_dich_vu($conn,$data,&$erro){
			unlink('../temp/upload/dichvu/'.$data['img_cu']);
			$stmt=$conn->prepare("UPDATE ".DB_DICHVU." SET ten_dv=:ten_dv,tom_tat=:tom_tat,gia_dv=:gia_dv,an_hien=:an_hien,img=:img,tieu_de=:tieu_de WHERE id=:id");
			$stmt->bindParam(":ten_dv",$data['ten_dich_vu'],PDO::PARAM_STR);
			$stmt->bindParam(":tom_tat",$data['tom_tat'],PDO::PARAM_STR);
			$stmt->bindParam(":gia_dv",$data['gia_dv'],PDO::PARAM_INT);
			$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_STR);
			$stmt->bindParam(":tieu_de",$data['tieu_de'],PDO::PARAM_STR);
			$stmt->bindParam(":img",$data['img'],PDO::PARAM_STR);
			$stmt->bindParam(":id",$data['id'],PDO::PARAM_INT);
			$stmt->execute();
			$bonhotam =$data['tmp_name'];
				move_uploaded_file($bonhotam,"../temp/upload/dichvu/".$data['img']);
			chuyentrang('list-dich-vu');
	}
	//5.delete dich vụ
	function delete_dich_vu($conn,$id){

		$data =lay_ten_img($conn,$id);
		$stmt= $conn->prepare("DELETE FROM ".DB_DICHVU." WHERE id=:id");
		$stmt->bindParam(":id",$id,PDO::PARAM_INT);
		$stmt->execute();
		unlink('../temp/upload/dichvu/'.$data['img']);
		chuyentrang("list-dich-vu");

	}

// END DỊCH VUJ0----------------------------
//load - thu vien anh
	function load_thu_vien_anh($conn,$id){
			$load= $conn->prepare("SELECT * FROM ".DB_THUVIEN." WHERE id=:cid");
			$load->bindParam(":cid",$id,PDO::PARAM_INT);
			$load->execute();
			$data= $load->fetch(PDO::FETCH_ASSOC);
			return $data;
	}
	function edit_thu_vien_anh($conn,$data){
			$stmt=$conn->prepare("UPDATE ".DB_THUVIEN." SET dia_diem=:dia_diem,an_hien=:an_hien WHERE id=:id");
			$stmt->bindParam(":dia_diem",$data['ten_tvien'],PDO::PARAM_STR);
			$stmt->bindParam(":an_hien",$data['an_hien'],PDO::PARAM_INT);
			$stmt->bindParam(":id",$data['cid'],PDO::PARAM_INT);
			$stmt->execute();
			chuyentrang('list-ten-thu-vien');
	}
	function delete_ten_thuvien($conn,$id){
		$stmt= $conn->prepare("DELETE FROM ".DB_THUVIEN." WHERE id=:id");
		$stmt->bindParam(":id",$id,PDO::PARAM_INT);
		$stmt->execute();
		chuyentrang("list-ten-thu-vien");
	}
// end load thu vien anh

		function list_lich_trinh($conn){
			$stmt=$conn->prepare('SELECT * FROM '.DB_LICHTRINH.' ORDER BY id ASC');
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		function list_lich_trinh_index($conn,$id){
			$stmt=$conn->prepare('SELECT * FROM '.DB_LICHTRINH.' WHERE id_ct_tour=:id_ct_tour ORDER BY id ASC');
			$stmt->bindParam(":id_ct_tour",$id,PDO::PARAM_STR);
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		function load_bg_index($conn,$id){
			$stmt=$conn->prepare('SELECT * FROM '.DB_BAOGOM.' WHERE id_chi_tiet_tour=:id_chi_tiet_tour ORDER BY id ASC');
			$stmt->bindParam(":id_chi_tiet_tour",$id,PDO::PARAM_STR);
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		function load_k_bg_index($conn,$id){
			$stmt=$conn->prepare('SELECT * FROM '.DB_K_BAOGOM.' WHERE id_chi_tiet_tour=:id_chi_tiet_tour ORDER BY id ASC');
			$stmt->bindParam(":id_chi_tiet_tour",$id,PDO::PARAM_STR);
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		function load_show_tentour($conn,$id){
			$stmt=$conn->prepare('SELECT * FROM '.DBTOUR.' WHERE id=:id');
			$stmt->bindParam(":id",$id,PDO::PARAM_INT);
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			return $data;
		}

 ?>



 <!-- font -end   -->
 <?php 
 	// 1...Load slider:
 	function load_slider_font_end($conn){
 			$load= $conn->prepare('SELECT * FROM '.DB_SLIDER.' ORDER BY id ASC');
			$load->execute();
			$data= $load->fetchAll(PDO::FETCH_ASSOC);
			return $data;
 	}
 	//2.load tour _index
 	function load_tour_index($conn){
 		$stmt=$conn->prepare('SELECT * FROM '.DBTOUR.' ORDER BY id ASC');
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $data;
 	}
 	
 	//3. load thu vien index
 	function load_thuvien_index_ten($conn){
 		$stmt=$conn->prepare('SELECT * FROM '.DB_THUVIEN.' ORDER BY id ASC');
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $data;
 	}
 	function load_img_thuvien_index_ten($conn,$id){
 		$stmt=$conn->prepare('SELECT * FROM '.DB_IMGTHUVIEN.' WHERE id_thu_vien=:id_thu_vien');
 		$stmt->bindParam(":id_thu_vien",$id,PDO::PARAM_INT);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $data;
 	}
 	//4..load dịch vụ trang dịch vụ
 		function load_dv_trang_dv($conn){
 			$stmt=$conn->prepare('SELECT * FROM '.DB_DICHVU.' ORDER BY id ASC');
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
 		}
	
 	//6.Load chi tiet tour **vớ vẩn88
 		function load_ct_tour_index($conn,$id){
 			$stmt=$conn->prepare('SELECT * FROM '.DBCT_TOUR.' WHERE id_tour=:id_tour');
 			$stmt->bindParam(":id_tour",$id,PDO::PARAM_INT);
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			return $data;
 		}
 		function load_data_ct_tour_index2($conn,$id){
 			$stmt=$conn->prepare('SELECT * FROM '.DBTOUR.' WHERE id=:id');
 			$stmt->bindParam(":id",$id,PDO::PARAM_INT);
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			return $data;
 		}

 	
 		

 //-------------------------------------------------
 	//1.Load menu loai tour
 		function load_menu_loai_tour($conn){
 			$stmt=$conn->prepare("SELECT * FROM ".DBLOAITOUR);
 			$stmt->execute();
 			$data= $stmt->fetchAll(PDO::FETCH_ASSOC);
 			return $data;
 		}
 	//2.load tour hành hương 
 		function load_tour_show($conn,$id){
 			$stmt=$conn->prepare('SELECT * FROM '.DBTOUR.' WHERE id_loai_tours=:id_loai_tours');
 			$stmt->bindParam(":id_loai_tours",$id,PDO::PARAM_INT);
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
 		}
 	//3.Load chi tiết tour
 		function load_ct_tour_show($conn,$id){
 			$stmt=$conn->prepare('SELECT * FROM gmt_chi_tiet_tour,gmt_tour WHERE id_tour=:id_tour and gmt_chi_tiet_tour.id_tour=gmt_tour.id');
 			$stmt->bindParam(":id_tour",$id,PDO::PARAM_INT);
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			return $data;
 		}
 		function  load_img_ct_tour_show($conn,$id){
 			$stmt=$conn->prepare('SELECT * FROM '.DB_IMGCTTOUR.' WHERE id_chi_tiet_tour=:id_chi_tiet_tour ');
 			$stmt->bindParam(":id_chi_tiet_tour",$id,PDO::PARAM_INT);
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $data;
 		}

 		
 	//4. load id_tour slider
 		function load_id_tour_slider($conn,$id){
 			$stmt=$conn->prepare('SELECT * FROM gmt_chi_tiet_tour,gmt_slider WHERE gmt_slider.id=:id and gmt_chi_tiet_tour.id=gmt_slider.id_chi_tiet_tour');
 			$stmt->bindParam(":id",$id,PDO::PARAM_INT);
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			return $data;
 		}

  ?>