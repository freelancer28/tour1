<?php 
session_start();
if (!isset($_SESSION['dh_level']) || $_SESSION['dh_level'] !=1) {
	header("location:login.php");
	exit();
}else{
 ?>

<?php 
	include("../config.php");
	include("../dh96_vendor/connect.php");
	include 'model/model.php';
	include('../dh96_vendor/function.php');
 ?>
 <!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="temp/css/style.css" />
    <link rel="stylesheet" href="temp/css/style_upload.css">
	<title>Admin Area :: Trang chính</title>
	<script  type="text/javascript" charset="utf-8" >
		function checkDelete(msg){
			if(window.confirm(msg) == true)
				return true;
			return false;
		}
	</script>
</head>
<body>
<div id="layout">
    <div id="top">
        Admin Area :: Trang chính
    </div>
	<div id="menu">
		<table width="100%">
			<tr>
				<td>
					<a href="index.php">Trang chính </a>|
					<a href="index.php?p=list-user">Quản lý users </a>|
					<a href="index.php?p=list-loai-tour">Quản lý loại tours </a>|
					<a href="index.php?p=list-tour">Quản lý tours </a>|
					<a href="index.php?p=list-ten-thu-vien">Quản lý thư viện </a>
				</td>
				<td align="right">
					Xin chào admin | <a href="index.php?p=logout">Logout</a>
				</td>
			</tr>
		</table>
	</div>
   
	<?php 

		if (isset($_GET['p'])) {
			$p=$_GET['p'];
			switch ($p) {
				case 'trang-chu':
					include 'module/dashbroad/index.php';
					break;

				//link loai tour
				case 'list-loai-tour':
					include 'module/loaitour/list_loai_tour.php';
					break;
				case 'add-loai-tour':
					include 'module/loaitour/add_loai_tour.php';
					break;
				case 'delete-loai-tour':
					include 'module/loaitour/delete_loai_tour.php';
					break;
				case 'edit-loai-tour':
					include 'module/loaitour/edit_loai_tour.php';
					break;

				//link tour
				case 'list-tour':
					include 'module/tour/list_tour.php';
					break;
				case 'add-tour':
					include 'module/tour/add_tour.php';
					break;
				case 'delete-tour':
					include 'module/tour/delete_tour.php';
					break;
				case 'edit-tour':
					include 'module/tour/edit_tour.php';
					break;
				//link chi-tiet_tour
				case 'list-ct-tour':
					include 'module/chitiettour/list_chi_tiet_tour.php';
					break;
				case 'list-bao-gom-ct-tour':
					include 'module/chitiettour/baogom/list_bg_ct_tour.php';
					break;
				case 'list-k-bao-gom-ct-tour':
					include 'module/chitiettour/khongbaogom/list_kbg_ct_tour.php';
					break;
				case 'list-img-ct-tour':
					include 'module/chitiettour/img/list_img_ct_tour.php';
					break;
				case 'add-ct-tour':
					include 'module/chitiettour/add_chi_tiet_tour.php';
					break;
				case 'delete-ct-tour':
					include 'module/chitiettour/delete_chi_tiet_tour.php';
					break;

				case 'delete-bao-gom-ct-tour':
					include 'module/chitiettour/baogom/delete_bg_ct_tour.php';
					break;
				case 'delete-k-bao-gom-ct-tour':
					include 'module/chitiettour/khongbaogom/delete_kbg_ct_tour.php';
					break;
				case 'delete-img-ct-tour':
					include 'module/chitiettour/img/delete_img_ct_tour.php';
					break;

				case 'edit-ct-tour':
					include 'module/chitiettour/edit_chi_tiet_tour.php';
					break;
				//slider 
				case 'list-slider':
					include 'module/slider/list.php';
					break;
				case 'add-slider':
					include 'module/slider/add.php';
					break;
				case 'delete-slider':
					include 'module/slider/delete.php';
					break;
				case 'edit-slider':
					include 'module/slider/edit.php';
					break;
				//thu -vien
				case 'list-thu-vien':
					include 'module/thuvien/list_thu_vien.php';
					break;
				case 'list-ten-thu-vien':
					include 'module/thuvien/list_ten_thu_vien.php';
					break;
				case 'edit-ten-thu-vien':
					include 'module/thuvien/edit_ten_thu_vien.php';
					break;
				case 'delete-ten-thu-vien':
					include 'module/thuvien/delete_ten_thu_vien.php';
					break;
				case 'add-thu-vien':
					include 'module/thuvien/add_thu_vien.php';
					break;
				case 'delete-thu-vien':
					include 'module/thuvien/delete_thu_vien.php';
					break;
				case 'edit-thu-vien':
					include 'module/thuvien/edit_thu_vien.php';
					break;
				//dich -vu
				case 'list-dich-vu':
					include 'module/dichvu/list_dich_vu.php';
					break;
				case 'add-dich-vu':
					include 'module/dichvu/add_dich_vu.php';
					break;
				case 'delete-dich-vu':
					include 'module/dichvu/delete_dich_vu.php';
					break;
				case 'edit-dich-vu':
					include 'module/dichvu/edit_dich_vu.php';
					break;
				//dich -vu
				case 'list-user':
					include 'module/user/list_user.php';
					break;
				case 'add-user':
					include 'module/user/add_user.php';
					break;
				case 'delete-user':
					include 'module/user/delete_user.php';
					break;
				case 'edit-user':
					include 'module/user/edit_user.php';
					break;

				case 'list-lich-trinh':
					include 'module/chitiettour/lichtrinh/list_lich_trinh.php';
					break;
				case 'delete-lich-trinh':
					include 'module/chitiettour/lichtrinh/delete_lich_trinh.php';
					break;

				case 'logout':
					include "logout.php";
					break;
				default:
					include 'module/dashbroad/index.php';
					break;
			}
		}else{
			include 'module/dashbroad/index.php';
		}
	 ?>
   
    <div id="bottom">
        Copyright © 2016 by Gà Hùng
    </div>
</div>
<script src="temp/js/jquery.js"></script>
<script src="temp/js/jquery.form.js"></script>
<script src="temp/js/main.js"></script>
</body>
</html>
<?php } ?>
