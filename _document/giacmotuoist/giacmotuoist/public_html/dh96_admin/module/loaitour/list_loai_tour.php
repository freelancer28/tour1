<?php 
    $data = list_loai_tour($conn);
    $stt=0;
 ?>
    <div id="main">
		<table class="list_table">
			<tr class="list_heading">
				<td class="id_col">STT</td>
				<td>Danh Mục Loại Tour</td>
                <td>Mô tả</td>
				<td class="action_col">Quản lý</td>
			</tr>
            <?php foreach ($data as  $value) { $stt++;?>
			<tr class="list_data">
                <td class="aligncenter"><?php echo $stt ?></td>
                <td class="list_td alignleft" style="padding-left: 10px"><?php echo $value['name'] ?></td>
                <td class="list_td alignleft" style="padding-left: 10px"><?php echo $value['mo_ta'] ?></td>
                <td class="list_td aligncenter">
                    <a href="index.php?p=edit-loai-tour&cid=<?php echo $value['id'] ?>"><img src="temp/images/edit.png" /></a>&nbsp;&nbsp;&nbsp;
                    <a onclick="return checkDelete('Bạn có muốn xóa hay không ?')"  href="index.php?p=delete-loai-tour&cid=<?php echo $value['id'] ?>"><img src="temp/images/delete.png" /></a>
                </td>
            </tr>
            <?php } ?>
			
		</table>
	</div>
    