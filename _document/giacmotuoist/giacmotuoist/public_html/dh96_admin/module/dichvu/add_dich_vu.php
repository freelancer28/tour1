<?php 
	$erro= null;
	if (isset($_POST['btnAdd'])) {
		if (empty($_POST['txtTenDv'])) {
			$erro = "Vui lòng nhập tên dịch vụ";
		}elseif (empty($_POST['txtTieuDe'])) {
			$erro = "Vui lòng nhập tiêu đề của dịch vụ";
		}elseif (empty($_POST['txtGia'])) {
			$erro = "Vui lòng nhập giá dịch vụ";
		}else if (empty($_FILES["imgdv"]['name'])) {
			$erro = "Vui lòng chọn  file hình!";
		}elseif (!Checkfile_img($_FILES["imgdv"]['name'])) {
			$erro = "Vui lòng chọn đúng đinh dạng file hình";
		}else{
			$file_name = $_FILES['imgdv']['name'];
			$file_name= convert_vi_to_en($file_name);
			$file_name = ChangeFileNameImg($file_name);


			$data=array(
			'tieu_de'=>$_POST['txtTieuDe'],
			'ten_dich_vu'=>$_POST['txtTenDv'],
			'gia_dv'=>$_POST['txtGia'],
			'tom_tat'=>$_POST['txtNoiDung'],
			'img'=>$file_name,
			'an_hien'=>$_POST['rdoAnhHien'][0],
			'tmp_name'=>$_FILES['imgdv']['tmp_name']
			);
			add_dv($conn,$data,$erro);
		}
	}
 ?>

<div id="main">
	<?php erro_messageshow($erro); ?>
		<form action="" method="POST" enctype="multipart/form-data" style="width: 650px;">
			<fieldset>
				<legend>Thông Tin dịch vụ </legend>
				
				<span class="form_label">Tên dịch vụ:</span>
				<span class="form_item">
					<input type="text" name="txtTenDv" class="textbox"  />
				</span><br />
				<span class="form_label">Tiêu đề:</span>
				<span class="form_item">
					<input type="text" name="txtTieuDe" class="textbox"  />
				</span><br />
				<span class="form_label">Giá dịch vụ:</span>
				<span class="form_item">
					<input type="text" name="txtGia" class="textbox"  />
				</span><br />
				<span class="form_label">Nội tóm tắt:</span>
				<span class="form_item">
					<textarea name="txtNoiDung" rows="8" class="textbox" >
						
					</textarea>
				</span><br />

				<span class="form_label">Hình đại diện:</span>
				<span class="form_item">
					<input type="file" name="imgdv" class="textbox"  />
				</span><br />
				<span class="form_label">Công bố:</span>
				<span class="form_item">
					<input type="radio" name="rdoAnhHien[]" value="1" checked="" /> Có 
					<input type="radio" name="rdoAnhHien[]" value="0"   /> Không
				</span><br />
				<span class="form_item">
				<span class="form_label"></span>
					<input type="submit" name="btnAdd" value="Thêm " class="button" />
				</span>
			</fieldset>
		</form>
	</div>