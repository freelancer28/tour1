<?php 
	$erro= null;
	$data_load_tour= load_cmb_tour($conn);
	if (isset($_POST['btnAdd'])) {
		if (empty($_POST['txtNoiDung'])) {
			$erro= "Vui lòng nhập nội dung tour!";
		}else{

			$data = array(
				'id_tour'=>$_POST['sltChiTietTour'],
				'noi_dung'=>$_POST['txtNoiDung'],
				'an_hien' =>$_POST['rdoAnhHien'][0]
			);
			add_chi_tiet_tour($conn,$data,$erro);
		}
	}
 ?>

<div id="main">
		<?php 
			erro_messageshow($erro);
		 ?>
		<form action="" method="POST" enctype="multipart/form-data" style="width: 650px;">
			<fieldset>
				<legend>Thông Tin Chi Tiết Tour</legend>
				<span class="form_label">Chọn tour:</span>
				<span class="form_item">
					<select name="sltChiTietTour" class="select">
						<?php 
							foreach ($data_load_tour as  $value) {
						 ?>
						<option <?php  selected_show('sltChiTietTour',$value['id']) ?> value="<?php echo $value['id'] ?>"><?php echo $value['ten_tour'] ?></option>	
						<?php } ?>
					</select>
				</span><br />
				<span class="form_label">Nội dung tin:</span>
				<span class="form_item">
					<textarea name="txtNoiDung" rows="8" class="textbox"></textarea>
				</span><br />
				<span class="form_label">Công bố:</span>
				<span class="form_item">
					<input type="radio" name="rdoAnhHien[]" value="1" checked="" /> Có 
					<input type="radio" name="rdoAnhHien[]" value="0"/> Không
				</span><br />
				<span class="form_label"></span>
				<span class="form_item">
					<input type="submit" name="btnAdd" value="Thêm " class="button" />
				</span>
			</fieldset>
		</form>
		<br><br>

		<?php 
			$erro1= null;
			if (isset($_POST['btnThemBG'])) {
				if (empty($_POST['txtNoiDung'])) {
					$erro1= "Vui lòng nhập nội dung bao gồm của tour!";
				}else{
					$data = array(
						'id_tour'=>$_POST['sltChiTietTour'],
						'noi_dung'=>$_POST['txtNoiDung']
					);
					add_bao_gom($conn,$data);
				}
			}
		 ?>
		<?php 
			erro_messageshow($erro1);
		 ?>
		<form action="" method="POST"  style="width: 650px;">
			<fieldset>
				<legend>Thông Tin Bao Gồm </legend>
				<span class="form_label">Chọn tour:</span>
				<span class="form_item">
					<select name="sltChiTietTour" class="select">
						<?php 
							foreach ($data_load_tour as  $value) {
						 ?>
						<option <?php  selected_show('sltChiTietTour',$value['id']) ?> value="<?php echo $value['id'] ?>"><?php echo $value['ten_tour'] ?></option>	
						<?php } ?>
					</select>
				</span><br />
				<span class="form_label">Nội dung :</span>
				<span class="form_item">
				<input type="text" name="txtNoiDung" class="textbox" tabindex="1" />
				</span><br />
				
				<span class="form_label"></span>
				<span class="form_item">
					<input type="submit" name="btnThemBG" value="Thêm Bao Gồm  " class="button" />
				</span>
			</fieldset>
		</form>
		<br><br>

		<?php 
			$erro2= null;
			if (isset($_POST['btnThemKBG'])) {
				if (empty($_POST['txtNoiDung2'])) {
					$erro2= "Vui lòng nhập nội dung không  bao gồm của tour!";
				}else{
					$data2 = array(
						'id_tour'=>$_POST['sltChiTietTour2'],
						'noi_dung'=>$_POST['txtNoiDung2']
					);
					add_k_bao_gom($conn,$data2);
				}
			}
		 ?>
		<?php 
			erro_messageshow($erro2);
		 ?>
		<form action="" method="POST"  style="width: 650px;">
			<fieldset>
				<legend>Thông Tin Không Bao Gồm </legend>
				<span class="form_label">Chọn tour:</span>
				<span class="form_item">
					<select name="sltChiTietTour2" class="select">
						<?php 
							foreach ($data_load_tour as  $value) {
						 ?>
						<option <?php  selected_show('sltChiTietTour',$value['id']) ?> value="<?php echo $value['id'] ?>"><?php echo $value['ten_tour'] ?></option>	
						<?php } ?>
					</select>
				</span><br />
				<span class="form_label">Nội dung :</span>
				<span class="form_item">
				<input type="text" name="txtNoiDung2" class="textbox" tabindex="1" />
				</span><br />
				
				<span class="form_label"></span>
				<span class="form_item">
					<input type="submit" name="btnThemKBG" value="Thêm Không Bao Gồm" class="button" />
				</span>
			</fieldset>
		</form>
		<br><br>
		<?php 
			$erro3= null;
			if (isset($_POST['btnUpload'])) {
				if ($_FILES['img_file']['name'][0] == "") {
					$erro3="Vui lòng chọn ít nhất 1 tấm hình!";
				}else{
					
					
					$data3= array(
							'id_tour'=>$_POST['sltChiTietTour3'],
							'img_name'=>$_FILES["img_file"]["name"],
							'img_tmp'=>$_FILES["img_file"]["tmp_name"],
							'an_hien'=>$_POST['rdoAnhHien'][0]
					);
					
					 add_ha_cttour($conn,$data3);
				}
			}
		 ?>
		<?php 
			erro_messageshow($erro3);
		 ?>
		<form action="" method="POST" enctype="multipart/form-data" style="width: 650px;" id="formUpload" >
			<fieldset>
				<legend>Thêm Hình Ảnh Cho Tour</legend>
				
				<span class="form_label">Thêm vào tour:</span>
				<span class="form_item">
						
					<select name="sltChiTietTour3" class="select">
						<?php 
							foreach ($data_load_tour as  $value) {
						 ?>
						<option <?php  selected_show('sltChiTietTour3',$value['id']) ?> value="<?php echo $value['id'] ?>"><?php echo $value['ten_tour'] ?></option>	
						<?php } ?>
					</select>	
							
					
				</span><br />
				<span class="form_label">Chọn file hình:</span>
				<span class="form_item">
					<input type="file" name="img_file[]" multiple="true" onchange="previewImg(event);" id="img_file" accept="image/*">
				</span><br />
				<span class="form_label"></span>
				<span class="form_item">
					<div class="box-preview-img"></div>
				</span><br />
				<span class="form_label">Công bố:</span>
				<span class="form_item">
					<input type="radio" name="rdoAnhHien[]" value="1" checked="" /> Có 
					<input type="radio" name="rdoAnhHien[]" value="0"/> Không
				</span><br />
				<span class="form_label"></span>
				<span class="form_item">
					<input  type="submit" name="btnUpload" value="Upload" class="button" />
					
				</span>
			</fieldset>
		</form>
		<br><br>
		
		<?php 
			$erro4= null;
			if (isset($_POST['btnLichTrinh'])) {
				if (empty($_POST['txtTieuDeLT'])) {
					$erro4= "Vui lòng nhập tiêu đề!";
				}elseif (empty($_POST['txtThoiGianLT'])) {
					$erro4= "Vui lòng nhập thời gian";
				}elseif (empty($_POST['txtNoiDungLT'])) {
					$erro4= "Vui lòng nhập nội dung !";
				}else if (empty($_FILES["imglt"]['name'])) {
					$erro4 = "Vui lòng chọn  file hình!";
				}else{
					$data4 = array(
						'id_tour'=>$_POST['sltChiTietTour4'],
						'noi_dung'=>$_POST['txtNoiDungLT'],
						'thoi_gian'=>$_POST['txtThoiGianLT'],
						'tieu_de'=>$_POST['txtTieuDeLT'],
						'img'=>$_FILES['imglt']['name'],
						'tmp_name'=>$_FILES['imglt']['tmp_name']

					);
					add_lich_trinh($conn,$data4,$erro4);
					
				}
			}
		?>
		<?php 
			erro_messageshow($erro4);
		 ?>

		<form action="" method="POST" enctype="multipart/form-data" style="width: 650px;">

			<fieldset>
			<legend>Thêm lịch trình tour</legend>
			<span class="form_label">Thêm vào tour:</span>
			<span class="form_item">
				<select name="sltChiTietTour4" class="select">
					<?php 
						foreach ($data_load_tour as  $value) {
					 ?>
					<option <?php  selected_show('sltChiTietTour4',$value['id']) ?> value="<?php echo $value['id'] ?>"><?php echo $value['ten_tour'] ?></option>	
					<?php } ?>
				</select>
			</span><br />
			<span class="form_label">Tiêu đề:</span>
			<span class="form_item">
				<input type="text" name="txtTieuDeLT" class="textbox" <?php load_laigiatridanhap('txtTieuDeLT')?>   />
			</span><br />
			<span class="form_label">Ngày mấy:</span>
			<span class="form_item">
				<input type="text" name="txtThoiGianLT" <?php load_laigiatridanhap('txtThoiGianLT')?> class="textbox"  />
			</span><br />
			<span class="form_label">Nội tóm tắt:</span>
			<span class="form_item">
				<textarea name="txtNoiDungLT" rows="8" class="textbox" >
					<?php 
						if (isset($_POST['txtNoiDungLT'])) {
							echo $_POST['txtNoiDungLT'];
						}
					 ?>
				</textarea>
			</span><br />
			<span class="form_label">Hình đại diện:</span>
			<span class="form_item">
				<input type="file" name="imglt" class="textbox"  />
			</span><br />
			<span class="form_label"></span>
			<span class="form_item">
				<input type="submit" name="btnLichTrinh" value="Thêm Lịch Trình" class="button" />
			</span>
			</fieldset>
		</form> 
	</div>