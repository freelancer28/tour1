<?php 
	if (isset($_GET['cid'])) {
		$cid=$_GET['cid'];
		$data_load_tour= load_cmb_tour($conn);
		$data_load=load_ct_tour_01($conn,$cid);
		if (isset($_POST['btnAdd'])) {
			if (empty($_POST['txtNoiDung'])) {
				$erro= "Vui lòng nhập nội dung tour!";
			}else{

				$data = array(
					'cid'=>$cid,
					'id_tour'=>$_POST['sltChiTietTour'],
					'noi_dung'=>$_POST['txtNoiDung'],
					'an_hien' =>$_POST['rdoAnhHien'][0]
				);
				edit_ct_tour($conn,$data);
				
			}
	}
 ?>
<div id="main">
		<form action="" method="POST" enctype="multipart/form-data" style="width: 650px;">
			<fieldset>
				<legend>Thông Tin Chi Tiết Tour</legend>
				<span class="form_label">Chọn tour:</span>
				<span class="form_item">
					<select name="sltChiTietTour" class="select">
						<?php 
							foreach ($data_load_tour as  $value) {
						 ?>
						<option <?php selected_show1('sltChiTietTour',$value['id'],$data_load['id_tour']);   ?> value="<?php echo $value['id'] ?>"><?php echo $value['ten_tour'] ?></option>	
						<?php } ?>
					</select>
				</span><br />
				<span class="form_label">Nội dung tin:</span>
				<span class="form_item">
					<textarea name="txtNoiDung" rows="8" class="textbox">
						<?php 
							echo $data_load['noi_dung'];
						 ?>
					</textarea>
				</span><br />
				<span class="form_label">Công bố:</span>
				<span class="form_item">
					<input type="radio" name="rdoAnhHien[]" value="1" checked="" /> Có 
					<input type="radio" name="rdoAnhHien[]" value="0"/> Không
				</span><br />
				<span class="form_label"></span>
				<span class="form_item">
					<input type="submit" name="btnAdd" value="Thêm " class="button" />
				</span>
			</fieldset>
		</form>
	</div>
	<?php }else{

		chuyentrang('trang-chu');
	} ?>