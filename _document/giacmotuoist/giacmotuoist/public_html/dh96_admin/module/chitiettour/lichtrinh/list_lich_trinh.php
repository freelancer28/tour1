<?php 
    $data=list_lich_trinh($conn);
    $stt=0;
 ?>
<div id="main">
        <table class="list_table">
            <tr class="list_heading">
                <td class="id_col">STT</td>
                <td>Tiêu đề</td>
                <td>Ngày</td>
                
                <td>Nội dung</td>

                <td>Hình đại diện</td>
                <td>Thuộc tour</td>
                <td class="action_col">Quản lý?</td>
            </tr>
            <?php foreach ($data as $value) { $stt++;?>
            <tr class="list_data">
                <td class="aligncenter"><?php echo $stt; ?></td>
                <td class="list_td aligncenter" style="width:90px"><?php echo $value['tieu_de'] ?></td>
                <td class="list_td aligncenter" style="width:60px" ><?php echo $value['ngay'] ?></td>
                
                <td class="list_td aligncenter" width="200"><?php echo $value['noi_dung'] ?></td>
                <td class="list_td aligncenter">
                    <img width="80px" src="../temp/upload/lichtrinh/<?php echo $value['img'] ?>">
                </td>
                <td class="list_td aligncenter" width="90">
                    <?php 
                        $name=load_show_tentour($conn,$value['id_ct_tour']);
                        echo $name['ten_tour'];
                    ?>
                </td>
                <td class="list_td aligncenter">
                    <a href="#"><img src="temp/images/edit.png" /></a>&nbsp;&nbsp;&nbsp;
                    <a onclick="return checkDelete('Bạn có muốn xóa hay không ?')" href="index.php?p=delete-lich-trinh&cid=<?php echo $value['id'] ?>"><img src="temp/images/delete.png" /></a>
                </td>
            </tr>
            <?php } ?>
            
        </table>
    </div>
    