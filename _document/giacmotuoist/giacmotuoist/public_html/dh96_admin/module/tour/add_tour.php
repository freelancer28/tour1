<?php 
$erro= null;
if (isset($_POST['btnAdd'])) {
	if (empty($_POST['txtTenTour'])) {
		$erro="Vui lòng nhập tên tour!";
	}elseif (empty($_POST['txtGia'])) {
		$erro="Vui lòng nhập giá tour!";
	}elseif (empty($_POST['txtNoiDen'])) {
		$erro="Vui lòng nhập nơi đến !";
	}else if (empty($_FILES["imgtour"]['name'])) {
		$erro = "Vui lòng chọn  file hình!";
	}elseif (!Checkfile_img($_FILES["imgtour"]['name'])) {
		$erro = "Vui lòng chọn đúng đinh dạng file hình";
	}else{
		$file_name = $_FILES['imgtour']['name'];
		$file_name= convert_vi_to_en($file_name);
		$file_name = ChangeFileNameImg($file_name);
	  				
		$data=array(
			'id_loai'=>$_POST['sltLoai_tour'],
			'ten_tour'=>$_POST['txtTenTour'],
			'thoi_gian'=>$_POST['txtThoiGian'],
			'gia_tour'=>$_POST['txtGia'],
			'dia_diem'=>$_POST['txtNoiDen'],
			'tom_tat'=>$_POST['txtNoiDung'],
			'img'=>$file_name,
			'an_hien'=>$_POST['rdoAnhHien'][0],
			'tmp_name'=>$_FILES['imgtour']['tmp_name']

		);
		add_tour($conn,$data,$erro);
		
	}
}

 ?>

<div id="main">
		<?php erro_messageshow($erro) ?>
		<form action="" method="POST" enctype="multipart/form-data" style="width: 650px;">
			<fieldset>
				<legend>Thông Tin Tour</legend>
				<span class="form_label">Loại tour:</span>
				<span class="form_item">
					<select name="sltLoai_tour" class="select">
							<?php $loaitour= load_cmb_loai_tour($conn);
								foreach ($loaitour as  $value) {
							 ?>
							<option value="<?php echo $value['id'] ?>" <?php  selected_show('sltLoai_tour',$value['id']) ?> ><?php echo $value['name'] ?> </option>
							<?php } ?>
					</select>
				</span><br />
				<span class="form_label">Tên tour:</span>
				<span class="form_item">
					<input type="text" name="txtTenTour" class="textbox" <?php load_laigiatridanhap('txtTenTour')?> />
				</span><br />
				<span class="form_label">Thời gian:</span>
				<span class="form_item">
					<input type="text" name="txtThoiGian" class="textbox" <?php load_laigiatridanhap('txtThoiGian')?> />
				</span><br />
				<span class="form_label">Giá tour:</span>
				<span class="form_item">
					<input type="text" name="txtGia" class="textbox" <?php load_laigiatridanhap('txtGia')?> />
				</span><br />
				<span class="form_label">Nơi đến:</span>
				<span class="form_item">
					<input type="text" name="txtNoiDen" class="textbox"  <?php load_laigiatridanhap('txtNoiDen')?> />
				</span><br />
				<span class="form_label">Nội tóm tắt:</span>
				<span class="form_item">
					<textarea name="txtNoiDung" rows="8" class="textbox" >
						<?php 
							if (isset($_POST['txtNoiDung'])) {
								echo $_POST['txtNoiDung'];
							}
						 ?>
					</textarea>
				</span><br />
				<span class="form_label">Hình đại diện:</span>
				<span class="form_item">
					<input type="file" name="imgtour" class="textbox"  />
				</span><br />
				<span class="form_label">Công bố:</span>
				<span class="form_item">
					<input type="radio" name="rdoAnhHien[]" value="1" checked="" /> Có 
					<input type="radio" name="rdoAnhHien[]" value="0" <?php checked_show('rdoAnhHien',0) ?>  /> Không
				</span><br />
				<span class="form_label"></span>
				<span class="form_item">
					<input type="submit" name="btnAdd" value="Thêm Tour" class="button" />
				</span>
			</fieldset>
		</form>
	</div>