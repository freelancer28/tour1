<?php 
    $data=list_tour($conn);
    $stt=0;
 ?>
<div id="main">
        <table class="list_table">
            <tr class="list_heading">
                <td class="id_col">STT</td>
                <td>Tên Tour</td>
                <td>Địa điểm</td>
                <td>Thời gian</td>
                <td>Giá</td>
                <td>Tóm tắt</td>
                <td>Ẩn Hiện</td>
                <td>Hình đại diện</td>
                <td>Loại tour</td>
                <td class="action_col">Quản lý?</td>
            </tr>
            <?php foreach ($data as $value) { $stt++;?>
            <tr class="list_data">
                <td class="aligncenter"><?php echo $stt; ?></td>
                <td class="list_td aligncenter" style="width:90px"><?php echo $value['ten_tour'] ?></td>
                <td class="list_td aligncenter" style="width:60px" ><?php echo $value['dia_diem'] ?></td>
                <td class="list_td aligncenter"><?php echo $value['thoi_gian'] ?></td>
                <td class="list_td aligncenter"><?php echo $value['gia_tour'] ?></td>
                <td class="list_td aligncenter"><?php echo $value['tom_tat'] ?></td>
                <td class="list_td aligncenter"><?php echo $value['an_hien'] ?></td>
                <td class="list_td aligncenter">
                    <img width="80px" src="../temp/upload/img_tour_index/<?php echo $value['img'] ?>">
                </td>
                <td class="list_td aligncenter">
                    <?php 
                        $name=load_show_loaitour($conn,$value['id_loai_tours']);
                        echo $name['name'];
                    ?>
                </td>
                <td class="list_td aligncenter">
                    <a href="index.php?p=edit-tour&cid=<?php echo $value['id'] ?>"><img src="temp/images/edit.png" /></a>&nbsp;&nbsp;&nbsp;
                    <a onclick="return checkDelete('Bạn có muốn xóa hay không ?')" href="index.php?p=delete-tour&cid=<?php echo $value['id'] ?>"><img src="temp/images/delete.png" /></a>
                </td>
            </tr>
            <?php } ?>
            
        </table>
    </div>
    