
 <?php 
 	$erro=null;
 	$erro1= null;
 	if (isset($_POST['btnThem'])) {
 		if (empty($_POST['txtTenThuVien'])) {
 			$erro="Vui lòng nhập tên thư viện";
 		}else{
 			$data= array(
				'ten_tvien'=>$_POST['txtTenThuVien'],
				'an_hien'=>$_POST['rdoAnhHien1'][0]
			);
			 add_thu_vien($conn,$data,$erro);

 		}
 	}



 	if (isset($_POST['btnUpload'])) {
 		if ($_FILES['img_file']['name'][0] == "") {
 			$erro1="Vui lòng chọn ít nhất 1 tấm hình!";
 		}else{
 			$data= array(
				'id_thu_vien'=>$_POST['sltDiaDiem'],
				'img_name'=>$_FILES["img_file"]["name"],
				'img_tmp'=>$_FILES["img_file"]["tmp_name"],
				'an_hien'=>$_POST['rdoAnhHien'][0]
			);
			add_img_thuvien($conn,$data);
 		}
 	}

  ?>
 
<div id="main">
		<?php 
			erro_messageshow($erro);
		 ?>
		<form action="" method="POST" enctype="multipart/form-data" style="width: 650px;"  >
			<fieldset>
				<legend>Thông Thư Viện</legend>
				<span class="form_label">Địa điểm:</span>
				<span class="form_item">
					<input type="text" name="txtTenThuVien" class="textbox" tabindex="1" />
				</span><br />
				
				<span class="form_label">Công bố:</span>
				<span class="form_item">
					<input type="radio" name="rdoAnhHien1[]" value="1" checked="" /> Có 
					<input type="radio" name="rdoAnhHien1[]" value="0"/> Không
				</span><br />
				<span class="form_label"></span>
				<span class="form_item">
					<input  type="submit" name="btnThem" value="Thêm" class="button" tabindex="2" />
					
				</span>
			</fieldset>
		</form>
		<br><br>  
		<?php erro_messageshow($erro1); ?>
		<form action="" method="POST" enctype="multipart/form-data" style="width: 650px;" id="formUpload" >
			<fieldset>
				<legend>Thêm Hình Ảnh Cho Thư Viện</legend>
				
				<span class="form_label">Thư viện có sẳn:</span>
				<span class="form_item">
					<select name="sltDiaDiem" class="select">
							 <?php 
								$data_load= load_thuvien($conn);
								if(count($data_load) == 0 ){
							 ?>
							<option value="none">Chưa có dữ liệu</option>
							<?php }else{ foreach ($data_load as  $value) {?>
							<option value="<?php echo $value['id'] ?>"><?php echo $value['dia_diem'] ?></option>
							<?php }} ?>
							
							
					</select>
				</span><br />
				<span class="form_label">Chọn file hình:</span>
				<span class="form_item">
					<input type="file" name="img_file[]" multiple="true" onchange="previewImg(event);" id="img_file" accept="image/*">
				</span><br />
				<span class="form_label"></span>
				<span class="form_item">
					<div class="box-preview-img"></div>
				</span><br />
				<span class="form_label">Công bố:</span>
				<span class="form_item">
					<input type="radio" name="rdoAnhHien[]" value="1" checked="" /> Có 
					<input type="radio" name="rdoAnhHien[]" value="0"/> Không
				</span><br />
				<span class="form_label"></span>
				<span class="form_item">
					<input  type="submit" name="btnUpload" value="Upload" class="button" />
					
				</span>
			</fieldset>
		</form> 
	</div>
	