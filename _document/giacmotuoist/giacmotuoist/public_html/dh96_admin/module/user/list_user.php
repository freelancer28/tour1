<div id="main">
        <table class="list_table">
            <tr class="list_heading">
                <td class="id_col">STT</td>
                <td>Username</td>
                <td>Email</td>
                <td>Điện thoại</td>
                <td>Level</td>
                <td class="action_col">Quản lý?</td>
            </tr>
            <?php 
                $stt=0;
                $data= list_user($conn);
                foreach ($data as $key => $value) { $stt++;
             ?>
            <tr class="list_data">
                <td class="aligncenter"><?php echo $stt; ?></td>
                <td class="list_td aligncenter"><?php echo $value['username'] ?></td>
                <td class="list_td aligncenter"><?php echo $value['email'] ?></td>
                <td class="list_td aligncenter"><?php echo $value['sdt'] ?></td>
                <td class="list_td aligncenter">
                    <?php if ($value['level'] == 0) { ?>
                    <span style="color: red; font-weight: bold;">Super Admin</span>
                    <?php }else{ ?>
                    <span style="color: blue"> Admin</span>
                    <?php } ?>
                </td>
                <td class="list_td aligncenter">
                    <a href="index.php?p=edit-user&cid=<?php echo $value['id'] ?>"><img src="temp/images/edit.png" /></a>&nbsp;&nbsp;&nbsp;
                    <a onclick="return checkDelete('Bạn có muốn xóa hay không ?')" href="index.php?p=delete-user&cid=<?php echo $value['id'] ?>"><img src="temp/images/delete.png" /></a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>