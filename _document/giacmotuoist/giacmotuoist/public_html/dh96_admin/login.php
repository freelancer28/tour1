<?php
session_start();
if (!isset($_SESSION['dh_user'])) {
    include("../config.php");
    include("../dh96_vendor/connect.php");
    include 'model/model.php';
    include('../dh96_vendor/function.php');
    $erro= null;
    if (isset($_POST['btnLogin'])) {
        if (empty($_POST['txtUser'])) {
            $erro ="Vui lòng nhập tên đăng nhập!";
        }elseif (empty($_POST['txtPass'])) {
            $erro ="Vui lòng nhập password!";
        }else{
            $user =$_POST['txtUser'];
            $password = md5($_POST['txtPass']);
            $data = array(
                'user'=>$user,
                'pass'=>$password
            );
           
            checkuser($conn,$data,$erro);

        }
    }
   
 ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="temp/css/style.css" />
	<title>Admin Area :: Login</title>
</head>
<body>
<div id="layout">
    <div id="top">
        Admin Area :: Login
    </div>
    <div id="main">
    <?php erro_messageshow($erro); ?>        
		<form action="" method="POST" style="width: 650px; margin: 30px auto;">
            <fieldset>
                <legend>Thông Tin Đăng Nhập</legend>                
				<table>
                    <tr>
                        <td class="login1"></td>
                        <td>
                            <span class="form_label">Username:</span>
                            <span class="form_item">
                                <input type="text" name="txtUser" class="textbox" />
                            </span><br />
                            <span class="form_label">Password:</span>
                            <span class="form_item">
                                <input type="password" name="txtPass" class="textbox" />
                            </span><br />
                            <span class="form_label"></span>
                            <span class="form_item">
                                <input type="submit" name="btnLogin" value="Đăng nhập" class="button" />
                            </span>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
    <div id="bottom">
        Copyright © 2016 by Gà Hùng
    </div>
</div>

</body>
</html>
<?php }else{
    header("location:index.php");
}

 ?>