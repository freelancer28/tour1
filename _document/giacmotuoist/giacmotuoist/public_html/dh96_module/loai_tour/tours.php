<?php 
		if (isset($_GET['p'])) {
 ?>
<section class="container site-content">
	<div class="row">
		<?php 
			if (isset($_GET['idlt'])) {
				$idlt=$_GET['idlt'];
		 ?>
		<div class="column ninecol">
			<?php 
							$data=load_tour_show($conn,$idlt);
							foreach ($data as $value) {
				 ?>
			<div class="items-list clearfix">
				<div class="full-tour clearfix">
					<div class="fivecol column">		
						<div class="content-slider-container tour-slider-container">
										<div class="featured-image">
								<a href="index.php?p=chi-tiet-tour&idctt=<?php echo $value['id']?>">
									<img width="550" height="413" src="temp/upload/img_tour_index/<?php echo $value['img'] ?>" class="attachment-extended wp-post-image" alt="image_12" />
								</a>
							</div>
							<div class="block-background layer-2">
								
							</div>
						</div>		
					</div>
					<div class="sevencol column last">
						<div class="section-title">
							<h1><a href="index.php?p=chi-tiet-tour&idctt=<?php echo $value['id']?>"><?php echo $value['ten_tour'] ?></a></h1>
						</div>
						<ul class="tour-meta">
							<li>
								<div class="colored-icon icon-2"></div>
								<strong>Địa điểm:</strong>
								<a href="#" rel="tag"><?php echo $value['dia_diem'] ?></a>
							</li>
							<li>
								<div class="colored-icon icon-1"><span></span></div>
								<strong>Thời gian:</strong> <?php echo $value['thoi_gian'] ?> ngày
							</li>
							<li><div class="colored-icon icon-3">
								<span></span></div><strong>Số tiền:</strong> <?php echo $value['gia_tour'] ?> VNĐ
							</li>
						</ul>
						<p><p><?php echo $value['tom_tat'] ?></p></p>
						<footer class="tour-footer">
							<a href="#booking-form" data-id="82" data-title="Samui Holidays" class="button small colorbox inline"><span>Đặt Tour</span></a>
							<!-- <a href="#question-form" data-id="82" data-title="Samui Holidays" class="button grey small colorbox inline"><span>Ask a Question</span></a> -->
							<a href="index.php?p=chi-tiet-tour&idctt=<?php echo $value['id']?>" type="button" class="button" >Chi tiết tour</a>
						</footer>
					</div>
					
				</div>
				<div class="clear"></div>	
			</div>
			<?php } ?>
			
			<nav class="pagination">
				<span class='page-numbers current'>1</span>
				<a class='page-numbers' href='#'>2</a>
				<a class='page-numbers' href='#'>3</a>
				<a class="next page-numbers" href="#"></a>
			</nav>
		</div> 
		<?php } ?>




		<aside class="column threecol last">
			<div class="widget widget_text">			
				<div class="textwidget">
					<div class="featured-image">
						<a href="index.html">
							<h3> giấc mơ tourist</h3>
						</a>
						<div class="media">
							<div class="row1">
								<span class="icon" id="diachi"></span>
								<span class="content-diachi" >320 Trần Bình Trọng P4 Quận 5 TP.Hồ Chí Minh </span>
							</div>
							<div class="row1">
								<span class="icon" id="sdt"></span>
								<span  class="content-sdt"  >091-777-2554</span>
							</div>
							<div class="row1">
								<span class="icon" id="email"></span>
								<span  class="content" >hungtd@gmail.com</span>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="widget widget_text">			
				<div class="textwidget">
					<div class="featured-image">
						<a href="http://themextemplates.com/demo/midway/tour/samui-holidays">
							<img id="img_tour" src="http://themextemplates.com/demo/midway/wp-content/uploads/2012/11/image_18.jpg" alt="" />
						</a>
					</div>
					<button type="button" class="button" id="btnVeMayBay">Đặt vé máy bay</button>
				</div>
			</div>
			
		</aside>
		<div class="hidden">
			<div class="booking-form popup-form" id="booking-form">
				<div class="section-title popup-title"><h4></h4></div>
				<form action="http://themextemplates.com/demo/midway/wp-admin/admin-ajax.php" method="POST" class="formatted-form ajax-form">			
					<p></p>
					<div class="message"></div>
					<div class="sixcol column ">
						<div class="field-container"><input type="text" id="full-name" name="full-name" value="" placeholder="Full Name"  /></div>
					</div>
					<div class="sixcol column last">
						<div class="clear"></div>
						<div class="field-container">
							<input type="text" id="email-address" name="email-address" value="" placeholder="Email Address"  />
						</div>
					</div>
					<div class="sixcol column ">
						<div class="field-container">
							<input type="text" id="departure-date" name="departure-date" value="" class="date-field" placeholder="Departure Date"  />
						</div>
					</div>
					<div class="sixcol column last">
						<div class="clear"></div>
						<div class="field-container"><input type="text" id="arrival-date" name="arrival-date" value="" class="date-field" placeholder="Arrival Date"  />
						</div>
					</div>
					<div class="sixcol column ">
						<div class="field-container"><input type="text" id="adults-number" name="adults-number" value="" placeholder="Adults Number"  /></div>
					</div>
					<div class="sixcol column last">
						<div class="clear"></div>
						<div class="field-container"><input type="text" id="kids-number" name="kids-number" value="" placeholder="Kids Number"  /></div>
					</div>			<input type="hidden" name="id" value="" class="popup-id" />
					<input type="hidden" name="slug" value="booking" />
					<input type="hidden" class="action" value="themex_form_submit" />
					<a class="submit-button button" href="#">Submit</a>
				</form>
			</div>
<!-- booking form -->
			<div class="question-form popup-form" id="question-form">
				<div class="section-title popup-title"><h4></h4></div>
				<form action="http://themextemplates.com/demo/midway/wp-admin/admin-ajax.php" method="POST" class="formatted-form ajax-form">
					<p></p><div class="message"></div><div class="sixcol column "><div class="field-container"><input type="text" id="full-name" name="full-name" value="" placeholder="Full Name"  /></div></div><div class="sixcol column last"><div class="clear"></div><div class="field-container"><input type="text" id="email-address" name="email-address" value="" placeholder="Email Address"  /></div></div><div class="clear"></div><div class="field-container"><textarea id="question" name="question" placeholder="Question" ></textarea></div>			<input type="hidden" name="id" value="" class="popup-id" />
					<input type="hidden" name="slug" value="question" />
					<input type="hidden" class="action" value="themex_form_submit" />
					<a class="submit-button button" href="#">Submit</a>
				</form>
			</div>
<!-- question form -->
		</div>							
	</div>		
</section>
			
<?php }else{
	chuyentrang('trang-chu');
} ?>

		<!-- content -->