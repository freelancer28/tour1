<section class="container site-content">

			<div class="row">

				<div class="eightcol column">

					<div class="fivecol column">

						<img class="alignnone size-medium wp-image-21 demo-image" title="image_1" src="temp/images/image_1.jpg" alt="" />

					</div>

					<div class="sevencol column last">

						<div class="section-title">

							<h1>Khám phá thế giới</h1>

						</div>

								Bạn đang tìm cho mình một chuyến đi du lịch nước ngoài với gia đình, bạn bè hoặc đồng nghiệp? Chúng tôi sẵn sàng cung cấp dịch vụ cho bạn. Nếu bạn còn băn khoăn về chất lượng dịch vụ, túi tiền phù hợp với bạn hay lựa chọn thời điểm tốt nhất cho chuyến đi du lịch trong, thì đây là hệ thống đáp ứng được mọi yêu cầu đó. Chúng tôi cam kết đưa ra chất lượng dịch vụ tốt nhất xứng đáng với chi phí bạn chi trả, ngoài việc lên lịch khởi hành cố định các ngày trong trong tháng, chúng tôi sẵn sàng tổ chức tour cho bạn vào bất kỳ thời điểm nào bạn muốn.

					</div>

					<div class="clear"></div>

				</div>

				<div class="fourcol column last">

					<div class="content-slider testimonials-slider">

						<ul>

							<li>

								<article class="testimonial">

									<div class="quote-text">

										<div class="block-background">

											Chúng tôi đã có chuyến đi tuần trăng mật tuyệt vời nhất ở Thái Lan nhờ bạn. Không có nghi ngờ chuyến đi vượt xa mong đợi của chúng tôi. Cảm ơn bạn!			</div>

									</div>

									<h6 class="quote-author">

										Gà Anh Hùng	

									</h6>

								</article>

							</li>

							<li>

								<article class="testimonial">

									<div class="quote-text">

										<div class="block-background">

											Cảm ơn bạn về chuyến đi kỳ diệu mà bạn sắp xếp ở Ấn Độ. Chúng tôi không bao giờ có thể cùng nhau lên kế hoạch cho chuyến thăm đó. Kinh ngạc!			</div>

									</div>

									<h6 class="quote-author">

										Gà Trụi Lông		</h6>

								</article>

							</li>

							

						</ul>



						<input type="hidden" class="slider-pause" value="0" />
						<input type="hidden" class="slider-speed" value="400" />

					</div>

				</div>

				<div class="clear"></div>

			</div>

</section>

<section class="container content-section">

	<div class="substrate section-substrate">

		<img src="temp/images/background_1.jpg" class="fullwidth" alt="" />

	</div>

	<div class="row">

		<div class="items-grid">

			<?php 

				$data_tour=load_tour_index($conn);

				foreach ($data_tour as $key => $value) {

						$stt=$key+1;

						if ($stt %4 !=0) {

			?>

			<div class="column threecol ">

				<div class="tour-thumb-container">

					<div class="tour-thumb">

						<a href="index.php?p=chi-tiet-tour&idctt=<?php echo $value['id'] ?>">

							<img width="440" height="330" src="temp/upload/img_tour_index/<?php echo $value['img']?>" class="attachment-preview wp-post-image" alt="<?php echo $value['img']?>" />

						</a>

						<div class="tour-caption">

							<h5 class="tour-title">

								<a href="index.php?p=chi-tiet-tour&idctt=<?php echo $value['id'] ?>"><?php echo $value['ten_tour'] ?> </a>

							</h5>

							<div class="tour-meta">

									<div class="tour-destination">

										<div class="colored-icon icon-2"></div>

										<a href="#" rel="tag"><?php  echo $value['dia_diem'] ?></a>

									</div>

									<div class="tour-duration"><?php  echo $value['thoi_gian'] ?> ngày</div>

							</div>

						</div>			

					</div>

					<div class="block-background"></div>

				</div>

			</div>				

			<?php }else{  ?>			

			<div class="column threecol last">

				<div class="tour-thumb-container">

					<div class="tour-thumb">

						<a href="index.php?p=chi-tiet-tour&idctt=<?php echo $value['id'] ?>"><img width="440" height="330" src="temp/upload/img_tour_index/<?php echo $value['img']?>" class="attachment-preview wp-post-image" alt="<?php echo $value['img']?>" /></a>

						<div class="tour-caption">

								<h5 class="tour-title"><a href="index.php?p=chi-tiet-tour&idctt=<?php echo $value['id'] ?>"><?php echo $value['ten_tour'] ?></a></h5>

								<div class="tour-meta">

									<div class="tour-destination">

										<div class="colored-icon icon-2"></div>

										<a href="" rel="tag"><?php  echo $value['dia_diem'] ?>

										</a>

									</div>

									<div class="tour-duration"><?php  echo $value['thoi_gian'] ?> ngày</div>

								</div>

						</div>			

					</div>

					<div class="block-background"></div>

				</div>

			</div>

			<?php }} ?>

			

			









		</div>

	</div>

</section>



<section class="container site-content">

	<div class="row">

		<div class="sixcol1 column">

			<div class="section-title">

				<h1>Thư Viện</h1>

			</div>

			<div class="items-grid">

				<?php 

				$data_ten_thuvien=load_thuvien_index_ten($conn);

				foreach ($data_ten_thuvien as $key => $value) {

						$stt1=$key+1;

						if ($stt1 %3 !=0) {

				?>

				<div class="column gallery-item fourcol ">

	

					<?php $data_img = load_img_thuvien_index_ten($conn,$value['id']) ?>

					<div class="featured-image">

						<a href="temp/demo/thuvien/anh1.jpg" class="colorbox " data-group="gallery-111" title="">

							<img width="440" height="330" src="temp/upload/thuvien/<?php echo $data_img[0]['img'] ?>" class="attachment-preview wp-post-image" alt="<?php echo $data_img[0]['img'] ?>" />		

						</a>

						<a class="featured-image-caption hidden-caption" href="#">

							<h6><?php echo $value['dia_diem'] ?></h6>

						</a>			

					</div>

					<?php foreach ($data_img as  $value) { ?>	

					<div class="hidden">	

								

						<a class="colorbox" href="temp/upload/thuvien/<?php echo $value['img'] ?>" data-group="gallery-111" title=""></a>

						

					</div>

					<?php } ?>

					<div class="block-background"></div>

				</div>

				<?php }else{  ?>	

				

				<div class="column gallery-item fourcol last">

					<?php $data_img = load_img_thuvien_index_ten($conn,$value['id']) ?>

					<div class="featured-image">

						<a href="temp/demo/thuvien/anh4.jpg" class="colorbox " data-group="gallery-109" title="">

							<img width="440" height="330" src="temp/upload/thuvien/<?php echo $data_img[0]['img'] ?>" class="attachment-preview wp-post-image" alt="<?php echo $data_img[0]['img'] ?>" />		

						</a>

						<a class="featured-image-caption hidden-caption" href="#">

							<h6><?php echo $value['dia_diem'] ?></h6>

						</a>			

					</div>

					<div class="hidden">	

						<?php foreach ($data_img as  $value) { ?>				

						<a class="colorbox" href="temp/upload/thuvien/<?php echo $value['img'] ?>" data-group="gallery-109" title=""></a>

						<?php } ?>

						

					</div>

					<div class="block-background"></div>

				</div>

				<?php }} ?>

				<div class="clear"></div>

			</div>

		</div>

		<div class="threecol column last">

			<div class="widget widget-subscribe">

				<div class="section-title">

					<h4>Bản tin</h4>

				</div>

					<p>

						Đăng ký bản tin của chúng tôi ngay bây giờ để cập nhật với những gì mới với Midway

					</p>

					<form action="" method="POST" class="ajax-form">

						<div class="message"></div>

						<div class="field-container">

							<input type="text" name="email" placeholder="Email Address" />

						</div>

						<input type="button" class="action" value="Gửi" id="btnQuangCao" />

					</form>

			</div>

			<div class="widget widget-twitter">

				<div class="section-title">

					<h4>Tin tức mới nhất</h4>

				</div>

				<div id="tweets">

					<ul>

						<li>

							<div class="colored-icon icon-4">

							</div>

							<p class="tweet">Dell Latitude E7440 14 inch và Dell Latitude E7240 12 inch là hai dòng Ultrabook của Dell Latitude có thiết kế chắc chắn,siêu bền và cấu hình khá mạnh</p>

						</li>

						<li>

							<div class="colored-icon icon-4">

							</div>

							<p class="tweet">Dell Latitude E7440 14 inch và Dell Latitude E7240 12 inch là hai dòng Ultrabook của Dell Latitude có thiết kế chắc chắn,siêu bền và cấu hình khá mạnh</p>

						</li>

						<li>

							<div class="colored-icon icon-4">

							</div>

							<p class="tweet">Dell Latitude E7440 14 inch và Dell Latitude E7240 12 inch là hai dòng Ultrabook của Dell Latitude có thiết kế chắc chắn,siêu bền và cấu hình khá mạnh</p>

						</li>

					</ul>

				</div>

			</div>

		</div>

		<div class="clear"></div>							

	</div>		

</section>