<div class="row subheader">
				<div class="threecol column subheader-block">
					<div class="tour-search-form placeholder-form">
						<div class="form-title">
							<h4>Tìm kiếm chuyến đi</h4>
						</div>
						<form role="search" method="get" action="">
							<div class="select-field">
								<span>Tất cả các địa điểm</span>
								<select name='destination' id='destination' class='postform' >
									<option value='0'>Tất cả các địa điểm</option>
									<option class="level-0" value="1">Đà Lạt</option>
									<option class="level-0" value="2">Nha Trang</option>
									<option class="level-0" value="3">Tây Nguyên</option>	
								</select>
							</div>
							<div class="select-field">
								<span>Loại tours</span>
								<select name='type' id='type' class='postform' >
									<option value='0'>Loại tours </option>
									<option class="level-0" value="1">Khách lẻ </option>
									<option class="level-0" value="2">Hành hương</option>
								</select>
							</div>
							<div class="field-container">
								<input type="text" name="date_dep" class="date-field" value="Ngày khởi hành" />
							</div>
							<div class="select-field">
								<span>Số ngày đi:</span>
								<select name='type' id='count-day' class='postform' >
									<option value='0'>Số ngày đi</option>
									<option class="level-0" value="1"> 1 Ngày </option>
									<option class="level-0" value="2"> 2 Ngày</option>
									<option class="level-0" value="3"> 3 Ngày</option>
								</select>
							</div>
							<div class="select-field">
								<span>Giá từ</span>
								<select name='type' id='money' class='postform' >
									<option value='0'>Giá từ </option>
									<option class="level-0" value="1"> 1.000.000 VNĐ </option>
									<option class="level-0" value="2"> 2.000.000 VNĐ</option>
									<option class="level-0" value="3"> 3.000.000 VNĐ</option>
								</select>
							</div>
							<div class="form-button">
								<div class="button-container">
									<a href="#" class="button submit-button">Chọn tour</a>
								</div>
							</div>
							<input type="hidden" name="s" value="" />
						</form>
					</div>
				<!-- tour search form -->								
				</div>
				<div class="ninecol column subheader-block last">
					<div class="main-slider-container content-slider-container">
						<div class="content-slider main-slider">
							<ul>
									<?php 
										$data= load_slider_font_end($conn);
										foreach ($data as $value) {
									 ?>
								<li>
									<div class="featured-image">
										<?php 
											$data_id_tour=load_id_tour_slider($conn,$value['id']);

										?>

										<a href="index.php?p=chi-tiet-tour&idctt=<?php echo $data_id_tour['id_tour'] ?>">
										<img width="824" height="370" src="temp/upload/slider/<?php echo $value['img']; ?>" class="attachment-large wp-post-image" alt="<?php echo $value['img']; ?>" />
										</a>
									</div>
								</li>
								<?php } ?>
								
							</ul>
							<div class="arrow arrow-left content-slider-arrow"></div>
							<div class="arrow arrow-right content-slider-arrow"></div>
							<input type="hidden" class="slider-pause" value="2000" />
							<input type="hidden" class="slider-speed" value="50" />
							<input type="hidden" class="slider-effect" value="fade" />
						</div>
						<div class="block-background layer-1"></div>
						<div class="block-background layer-2"></div>
					</div>
				</div>
			</div>
