<?php 
	if (isset($_GET['p'])) {
			if (isset($_GET['idctt'])) {
				$idctt=$_GET['idctt'];
				$data=load_ct_tour_show($conn,$idctt);
 ?>
<section class="container site-content">
	
	<div class="row">
		<div class="full-tour clearfix">	
			<div class="sixcol column">		
				<div class="content-slider-container tour-slider-container">
					<div class="content-slider tour-slider">
						<ul>
							<?php 
								$data_img=load_img_ct_tour_show($conn,$idctt);
								foreach ($data_img as $value) {
							 ?>
							<li><img src="temp/upload/img_ct_tour/<?php echo $value['img'] ?>" alt="" /></li>
							<?php } ?>
						</ul>
						<div class="arrow arrow-left content-slider-arrow"></div>
						<div class="arrow arrow-right content-slider-arrow"></div>
						<input type="hidden" class="slider-speed" value="400" />
						<input type="hidden" class="slider-pause" value="0" />
					</div>
					<div class="block-background layer-1"></div>
					<div class="block-background layer-2"></div>
				</div>		
			</div>
			<div class="fix sixcol column last fix">
				<div class="section-title">
					<h1><?php echo  $data['ten_tour'] ?></h1>
				</div>
				<ul class="tour-meta">
					<li>
						<div class="colored-icon icon-2"></div>
						<strong>Điểm đến:</strong>
						<a href="" rel="tag"><?php echo $data['dia_diem'] ?></a>
					</li>
					<li>
						<div class="colored-icon icon-1"><span></span></div>
						<strong>Thời gian:  </strong> <?php echo  $data['thoi_gian'] ?>  days
					</li>
					<li>
						<div class="colored-icon icon-3"><span></span></div>
						<strong>Giá:  </strong><?php echo $data['gia_tour'] ?>  VNĐ
					</li>
				</ul>
				<p><?php echo $data['tom_tat'] ?></p>
				<p><?php echo $data['noi_dung'] ?></p>				
				<footer class="tour-footer">
						<a href="#booking-form" data-id="82" data-title="Samui Holidays" class="button small colorbox inline"><span>Đặt Tour</span></a>
						<a href="#question-form" data-id="82" data-title="Samui Holidays" class="button grey small colorbox inline"><span>Đặt câu hỏi</span></a>
				</footer>
			</div>
		</div>
		<div class="hidden">
			<div class="booking-form popup-form" id="booking-form">
				<div class="section-title popup-title"><h4></h4></div>
				<form action="" method="POST" class="formatted-form ajax-form">			
					<p></p><div class="message"></div>
					<div class="sixcol column ">
						<div class="field-container"><input type="text" id="full-name" name="full-name" value="" placeholder="Họ tên"  /></div>
					</div>
					<div class="sixcol column last">
						<div class="clear"></div>
						<div class="field-container">
							<input type="text" id="email-address" name="email-address" value="" placeholder="Địa chỉ email"  />
						</div>
					</div>
					<div class="sixcol column ">
						<div class="field-container">
							<input type="text" id="departure-date" name="departure-date" value="" class="date-field" placeholder="Ngày khởi hành"  />
						</div>
					</div>
					<div class="sixcol column ">
						<div class="field-container">
							<input type="text" id="adults-number" name="adults-number" value="" placeholder="Số người"  />
						</div>
					</div>
					<div class="sixcol column last">
						<div class="clear"></div>
						<div class="field-container"><input type="text" id="kids-number" name="kids-number" value="" placeholder="Số trẻ em"  />
						</div>
					</div>			
					<input type="hidden" name="id" value="" class="popup-id" />
					<input type="hidden" name="slug" value="booking" />
					<input type="hidden" class="action" value="themex_form_submit" />
					<a class="submit-button button" href="#">Gửi</a>
				</form>
			</div>
	<!-- booking form -->
			<div class="question-form popup-form" id="question-form">
				<div class="section-title popup-title"><h4></h4></div>
				<form action="" method="POST" class="formatted-form ajax-form">
					<p></p>
					<div class="message"></div>
					<div class="sixcol column ">
						<div class="field-container">
							<input type="text" id="full-name" name="Họ và tên" value="" placeholder="Full Name"  />
						</div>
					</div>
					<div class="sixcol column last">
						<div class="clear"></div>
						<div class="field-container"><input type="text" id="email-address" name="email-address" value="" placeholder="Địa chỉ email"  /></div>
					</div>
					<div class="clear"></div>
					<div class="field-container">
						<textarea id="question" name="question" placeholder="Câu hỏi?" ></textarea>
					</div>			
					<input type="hidden" name="id" value="" class="popup-id" />
					<input type="hidden" name="slug" value="question" />
					<input type="hidden" class="action" value="themex_form_submit" />
					<a class="submit-button button" href="#">Gửi</a>
				</form>
			</div>
	<!-- question form -->
		</div>
		<div class="sixcol column">
			<div class="tour-itinerary">
				<?php 
					$data_lt=list_lich_trinh_index($conn,$idctt);
					foreach ($data_lt as  $value) {
					
				 ?>
				<div class="tour-day">
					<div class="tour-day-number">
						<h5>Ngày <?php echo $value['ngay'] ?></h5>
					</div>
					<div class="tour-day-text clearfix">
						<div class="bubble-corner"></div>
						<div class="bubble-text">
							<div class="column fivecol">
								<div class="featured-image">
									<img src="temp/upload/lichtrinh/<?php echo $value['img'] ?>" alt="" class="fullwidth" />
								</div>
							</div>
							<div class="column sevencol last">
								<h5><?php echo $value['tieu_de'] ?></h5>
									<?php echo $value['noi_dung'] ?>
							</div>
						</div>
					</div>
				</div>
				<?php }?>
			</div>
		</div>
		<div class="sixcol column last">
			<ul class="title-right">
					<li>
						<strong>Giá tour bao gồm:</strong> 
						<ul class="danh-sach-con">
							<?php 
								$databg=load_bg_index($conn,$idctt);
								foreach ($databg as $value) {
							 ?>
							<li><?php echo $value['noi_dung'] ?></li>
							<?php } ?>

						</ul>
					</li>
			</ul>
			<ul class="title-right">
					<li>
						<strong>Giá tour không bao gồm:</strong> 
						<ul class="danh-sach-con">
							<?php 
								$databg=load_k_bg_index($conn,$idctt);
								foreach ($databg as $value) {
							 ?>
							<li><?php echo $value['noi_dung'] ?></li>
							<?php } ?>

						</ul>
					</li>
			</ul>
		</div>
		<div class="clear"></div>
	

		<!-- <div class="related-tours clearfix">
			<div class="section-title">
				<h1>Các chuyến liên quan</h1>
			</div>
			<div class="items-grid">
				<div class="column threecol ">
					<div class="tour-thumb-container">
						<div class="tour-thumb">
							<a href="">
								<img width="440" height="330" src="demo/tours/tour1.jpg" class="attachment-preview wp-post-image" alt="image_15" />
							</a>
							<div class="tour-caption">
								<h5 class="tour-title"><a href="http://themextemplates.com/demo/midway/tour/moraine-lake">Đà Lạt</a></h5>
								<div class="tour-meta">
									<div class="tour-destination">
										<div class="colored-icon icon-2"></div>
										<a href="http://themextemplates.com/demo/midway/destinations/canada" rel="tag">Hàn Xẻn</a>
									</div>
									<div class="tour-duration">8 ngày</div>
								</div>
							</div>			
						</div>
						<div class="block-background"></div>
					</div>
				</div>
				<div class="column threecol ">
					<div class="tour-thumb-container">
						<div class="tour-thumb">
							<a href="">
								<img width="440" height="330" src="demo/tours/tour1.jpg" class="attachment-preview wp-post-image" alt="image_15" />
							</a>
							<div class="tour-caption">
								<h5 class="tour-title"><a href="http://themextemplates.com/demo/midway/tour/moraine-lake">Đà Lạt</a></h5>
								<div class="tour-meta">
									<div class="tour-destination">
										<div class="colored-icon icon-2"></div>
										<a href="http://themextemplates.com/demo/midway/destinations/canada" rel="tag">Hàn Xẻn</a>
									</div>
									<div class="tour-duration">8 ngày</div>
								</div>
							</div>			
						</div>
						<div class="block-background"></div>
					</div>
				</div>
				<div class="column threecol ">
					<div class="tour-thumb-container">
						<div class="tour-thumb">
							<a href="">
								<img width="440" height="330" src="demo/tours/tour1.jpg" class="attachment-preview wp-post-image" alt="image_15" />
							</a>
							<div class="tour-caption">
								<h5 class="tour-title"><a href="http://themextemplates.com/demo/midway/tour/moraine-lake">Đà Lạt</a></h5>
								<div class="tour-meta">
									<div class="tour-destination">
										<div class="colored-icon icon-2"></div>
										<a href="http://themextemplates.com/demo/midway/destinations/canada" rel="tag">Hàn Xẻn</a>
									</div>
									<div class="tour-duration">8 ngày</div>
								</div>
							</div>			
						</div>
						<div class="block-background"></div>
					</div>
				</div>
				<div class="column threecol last">
					<div class="tour-thumb-container">
						<div class="tour-thumb">
							<a href="#">
								<img width="440" height="330" src="demo/tours/tour32.jpg" class="attachment-preview wp-post-image" alt="image_9" />
							</a>
							<div class="tour-caption">
								<h5 class="tour-title">
									<a href="#">Lào Cai</a>
								</h5>
								<div class="tour-meta">
									<div class="tour-destination">
										<div class="colored-icon icon-2"></div>
										<a href="#" rel="tag">Hồng Công</a>
									</div>
									<div class="tour-duration">5 ngày</div>
								</div>
							</div>			
						</div>
						<div class="block-background"></div>
					</div>
				</div>
				<div class="clear"></div>	
			</div>
		</div> -->
	<!-- related tours -->
	</div>	
	
</section>
<?php }else{
	chuyentrang('trang-chu');
} ?>
		<!-- content -->
<?php }else{

	chuyentrang('trang-chu');
} ?>