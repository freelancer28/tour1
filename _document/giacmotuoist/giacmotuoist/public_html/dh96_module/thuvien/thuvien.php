<section class="container site-content">
			<div class="row">
				
				<div class="items-grid">
					<?php 
					$data_ten_thuvien=load_thuvien_index_ten($conn);
					foreach ($data_ten_thuvien as $key => $value) {
							$stt1=$key+1;
							if ($stt1 %4 !=0) {
					?>
					<div class="column gallery-item threecol ">
		
						<?php $data_img = load_img_thuvien_index_ten($conn,$value['id']) ?>
						<div class="featured-image">
							<a href="temp/demo/thuvien/anh1.jpg" class="colorbox " data-group="gallery-111" title="">
								<img width="440" height="330" src="temp/upload/thuvien/<?php echo $data_img[0]['img'] ?>" class="attachment-preview wp-post-image" alt="<?php echo $data_img[0]['img'] ?>" />		
							</a>
							<a class="featured-image-caption hidden-caption" href="#">
								<h6><?php echo $value['dia_diem'] ?></h6>
							</a>			
						</div>
						<?php foreach ($data_img as  $value) { ?>	
						<div class="hidden">	
									
							<a class="colorbox" href="temp/upload/thuvien/<?php echo $value['img'] ?>" data-group="gallery-111" title=""></a>
							
						</div>
						<?php } ?>
						<div class="block-background"></div>

					</div>
					<?php }else{  ?>	
					
					<div class="column gallery-item threecol last">
						<?php $data_img = load_img_thuvien_index_ten($conn,$value['id']) ?>
						<div class="featured-image">
							<a href="temp/demo/thuvien/anh4.jpg" class="colorbox " data-group="gallery-109" title="">
								<img width="440" height="330" src="temp/upload/thuvien/<?php echo $data_img[0]['img'] ?>" class="attachment-preview wp-post-image" alt="<?php echo $data_img[0]['img'] ?>" />		
							</a>
							<a class="featured-image-caption hidden-caption" href="#">
								<h6><?php echo $value['dia_diem'] ?></h6>
							</a>			
						</div>
						<div class="hidden">	
							<?php foreach ($data_img as  $value) { ?>				
							<a class="colorbox" href="temp/upload/thuvien/<?php echo $value['img'] ?>" data-group="gallery-109" title=""></a>
							<?php } ?>
							
						</div>
						<div class="block-background"></div>
					</div>
					<div class="clear"></div>
					<?php }} ?>
					<div class="clear"></div>

				</div>
			</div>		
		</section>