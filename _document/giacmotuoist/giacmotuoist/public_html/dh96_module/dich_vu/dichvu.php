<?php 
	$data= load_dv_trang_dv($conn);
 ?>
<section class="container site-content">
			<div class="row">
				<div class="tabs-container vertical-tabs">
					<div class="column threecol tabs">
						<ul>
							<?php foreach ($data as $value) { ?>
							<li><a href="#"><?php echo $value['ten_dv'] ?></a></li>
							<?php } ?>
						</ul>
					</div>
					<div class="panes column ninecol last">
						<?php foreach ($data as $value) { ?>
						<div class="pane">
							<div class="sevencol column">
								<h3><?php echo $value['tieu_de'] ?></h3>
								<p><strong><?php echo $value['tom_tat'] ?></strong></p>
								<!-- <p>Nhật Bản bóng đá thông minh cá cược chỉ mất một thời gian. Mauris venenatis congue Odio urna, bibendum ut interdum eget, suscipit, và ham muốn. Mỗi chuối, Enim eu ut libero eleifend, elit molestie leo, chocolate lobortis risus urna id velit. Nhưng bây giờ mà một số Performance Cục lớn nhất nhưng không có giá trị nó. Khi cuộc sống rất, Để ngày mai là nỗi đau.</p> -->
								<a href="index.php?p=lien-he" target="_self" class="button medium primary">Liên Hệ</a>
							</div>
							
							<div class="fivecol column last">
								<img class="aligncenter" alt="" src="temp/upload/dichvu/<?php echo $value['img'] ?>" />
							</div>
							<div class="clear"></div>
						</div>
						<?php } ?>
						
					</div>
				</div>
				<div class="fivecol column">
					<div class="section-title">
						<h4>Ý kiến khách hàng</h4>
					</div>
					<div class="staff-block">
						<div class="fourcol column">
							<div class="featured-image">
								<img src="demo/Dichvu/image_26.jpg" alt="" />
							</div>
						</div>
						<div class="eightcol column last">
							<h5>Mark Templeton</h5>
							<p>Fusce một chỉ, không dui Imperdiet risus. Donec consectetur metus sed velit placerat pulvinar. Thậm chí sau khi sự tự do của các nước, tristique eu, placerat consectetur Mattis một con sư tử. Mới nhất Playstation zero.</p>
							<a href="#" target="_self" class="button  grey">Liên hệ</a>
						</div>
						<div class="clear"></div>&nbsp;<br />

						<div class="fourcol column">
							<div class="featured-image">
								<img src="demo/Dichvu/image_26.jpg" alt="" />
							</div>
						</div>
						<div class="eightcol column last">
							<h5>Mark Templeton</h5>
							<p>Fusce một chỉ, không dui Imperdiet risus. Donec consectetur metus sed velit placerat pulvinar. Thậm chí sau khi sự tự do của các nước, tristique eu, placerat consectetur Mattis một con sư tử. Mới nhất Playstation zero.</p>
							<a href="#" target="_self" class="button  grey">Liên hệ</a>
						</div>

						<div class="clear"></div>

					</div>
				</div>
				<div class="fourcol column">
					<div class="section-title">
						<h4>Tại sao chọn chúng tôi</h4>
					</div>
					<div class="toggle">
						<div class="toggle-title">Không phải thiết kế lịch trình</div>
						<div class="toggle-content">Bạn thường phải lên kế hoạch cụ thể trước mỗi chuyến đi như điểm tham quan, các cung đường, chỗ ăn nghỉ. Nếu đặt tour, bạn sẽ không cần bận tâm về những điều trên. Việc duy nhất cần làm là mua một tour có sẵn hay trình bày ý tưởng với công ty du lịch, họ sẽ cung cấp lịch trình theo mong muốn của bạn.</div>
					</div>
					
					<div class="toggle">
						<div class="toggle-title">Không phải thiết kế lịch trình</div>
						<div class="toggle-content">Bạn thường phải lên kế hoạch cụ thể trước mỗi chuyến đi như điểm tham quan, các cung đường, chỗ ăn nghỉ. Nếu đặt tour, bạn sẽ không cần bận tâm về những điều trên. Việc duy nhất cần làm là mua một tour có sẵn hay trình bày ý tưởng với công ty du lịch, họ sẽ cung cấp lịch trình theo mong muốn của bạn.</div>
					</div>
					
					<div class="toggle">
						<div class="toggle-title">Không phải thiết kế lịch trình</div>
						<div class="toggle-content">Bạn thường phải lên kế hoạch cụ thể trước mỗi chuyến đi như điểm tham quan, các cung đường, chỗ ăn nghỉ. Nếu đặt tour, bạn sẽ không cần bận tâm về những điều trên. Việc duy nhất cần làm là mua một tour có sẵn hay trình bày ý tưởng với công ty du lịch, họ sẽ cung cấp lịch trình theo mong muốn của bạn.</div>
					</div>
					
					<div class="toggle">
						<div class="toggle-title">Không phải thiết kế lịch trình</div>
						<div class="toggle-content">Bạn thường phải lên kế hoạch cụ thể trước mỗi chuyến đi như điểm tham quan, các cung đường, chỗ ăn nghỉ. Nếu đặt tour, bạn sẽ không cần bận tâm về những điều trên. Việc duy nhất cần làm là mua một tour có sẵn hay trình bày ý tưởng với công ty du lịch, họ sẽ cung cấp lịch trình theo mong muốn của bạn.</div>
					</div>
					
				</div>
				<div class="threecol column last">
					<div class="section-title">
						<h4>Liên hệ</h4>
					</div>
					<div class="widget widget_text">			
						<div class="textwidget">
							<div class="featured-image">
								<a href="index.html">
									<h3> giấc mơ tourist</h3>
								</a>
								<div class="media">
									<div class="row1">
										<span class="icon" id="diachi"></span>
										<span class="content-diachi" >320 Trần Bình Trọng P4 Quận 5 TP.Hồ Chí Minh </span>
									</div>
									<div class="row1">
										<span class="icon" id="sdt"></span>
										<span  class="content-sdt"  >091-777-2554</span>
									</div>
									<div class="row1">
										<span class="icon" id="email"></span>
										<span  class="content" >hungtd@gmail.com</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>							
			</div>		
		</section>