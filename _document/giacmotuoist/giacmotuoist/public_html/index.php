
<?php 
	include'config.php';
	include'dh96_vendor/connect.php';
	include'dh96_admin/model/model.php';
	include'dh96_vendor/function.php';
 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Giấc Mơ Tuorist </title>	

	<!--CSS  -->
	<link rel='stylesheet' id='colorbox-css'  href='temp/js/colorbox/colorbox.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='jquery-ui-datepicker-css'  href='temp/framework/assets/css/datepicker.css' type='text/css' media='all' />
	<link rel='stylesheet' id='general-css'  href='temp/css/style.css' type='text/css' media='all' />
	<!-- End css -->


	<!-- javarcrip -->
	<script type='text/javascript' src='temp/js/jquery.js'></script>
	<script type='text/javascript' src='temp/js/jquery-migrate.min.js'></script>
	<script type='text/javascript' src='temp/js/comment-reply.min.js'></script>
	<script type='text/javascript' src='temp/js/jquery.hoverIntent.min.js'></script>
	<script type='text/javascript' src='temp/js/jquery.ui.touchPunch.js'></script>
	<script type='text/javascript' src='temp/js/colorbox/jquery.colorbox.min.js'></script>
	<script type='text/javascript' src='temp/js/jquery.placeholder.min.js'></script>
	<script type='text/javascript' src='temp/js/jquery.themexSlider.js'></script>
	<script type='text/javascript' src='temp/js/jquery.textPattern.js'></script>
	<!-- End javarcrip -->

	<script type='text/javascript'>
/* <![CDATA[ */
var labels = {"dateFormat":"dd\/mm\/yy","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesMin":["Su","Mo","Tu","We","Th","Fr","Sa"],"monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"firstDay":"1","prevText":"Prev","nextText":"Next"};
/* ]]> */
</script>
<script type='text/javascript' src='temp/js/general.js'></script>
<link rel="shortcut icon" href="temp/images/favicon.ico" /><style type="text/css">body, .site-container{}body, input, select, textarea{font-family:Open Sans, Arial, Helvetica, sans-serif;}h1,h2,h3,h4,h5,h6, .button, .header-menu a, .woocommerce a.button,.woocommerce button.button,.woocommerce input.button,.woocommerce #respond input#submit,.woocommerce #content input.button,.woocommerce-page a.button,.woocommerce-page button.button,.woocommerce-page input.button,.woocommerce-page #respond input#submit,.woocommerce-page #content input.button{font-family:Signika, Arial, Helvetica, sans-serif;}a, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, .header-menu ul ul a:hover, .header-menu > div > ul > li.current-menu-item > a,.header-menu > div > ul > li.current-menu-parent > a,.header-menu > div > ul > li.hover > a,.header-menu > div > ul > li:hover > a{color:#FF9000;}input[type="submit"], input[type="button"], .button, .colored-icon, .widget_recent_comments li:before, .ui-slider .ui-slider-range, .tour-itinerary .tour-day-number h5, .testimonials-slider .controls a.current, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit.alt, .woocommerce #content input.button.alt, .woocommerce-page a.button.alt, .woocommerce-page button.button.alt, .woocommerce-page input.button.alt, .woocommerce-page #respond input#submit.alt, .woocommerce-page #content input.button.alt, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce #content input.button.alt:hover, .woocommerce-page a.button.alt:hover, .woocommerce-page button.button.alt:hover, .woocommerce-page input.button.alt:hover, .woocommerce-page #respond input#submit.alt:hover, .woocommerce-page #content input.button.alt:hover{background-color:#FF9000;}.header .logo a, .header .header-text h5, .header .social-links span, .header-menu a, .header-menu a span, .site-footer .row, .site-footer a, .header-widgets .widget, .header-widgets .widget a, .header-widgets .section-title h3{color:#FFFFFF;}.header-menu ul ul li, .header-menu > div > ul > li.current-menu-item > a, .header-menu > div > ul > li.current-menu-parent > a, .header-menu > div > ul > li.hover > a, .header-menu > div > ul > li:hover > a{background-color:#FFFFFF;}::-moz-selection{background-color:#FF9000;}::selection{background-color:#FF9000;}</style><script type="text/javascript">
			WebFontConfig = {google: { families: [ "Signika:400,600","Open Sans:400,400italic,600" ] } };
			(function() {
				var wf = document.createElement("script");
				wf.src = ("https:" == document.location.protocol ? "https" : "http") + "://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js";
				wf.type = "text/javascript";
				wf.async = "true";
				var s = document.getElementsByTagName("script")[0];
				s.parentNode.insertBefore(wf, s);
			})();
			</script>	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>
<body class="home page page-id-96 page-template-default">
	<div class="container site-container">
		<header class="container site-header">
			<div class="substrate top-substrate">
				<img src="temp/images/site_bg.jpg" class="fullwidth" alt="" />
			</div>
			<!-- background -->
			<div class="row supheader">
				<div class="logo">
					<a href="index.php?p=trang-chu" rel="home">
						<img src="temp/images/logo.png" alt="Midway" />
					</a>
				</div>
			<!-- logo -->
				<div class="social-links">
					<a class="facebook" href="https://www.facebook.com/hoangngoc.tuananh.9" target="_blank" title="Facebook"></a>
					
					<a class="vimeo" href="#" target="_blank" title="Vimeo"></a>
				</div>
			<!-- social links -->
				<nav class="header-menu">
					<div class="menu">
						<ul id="menu-main-menu" class="menu">
							<li id="menu-item-97" class="menu-item menu-item-type-post_type menu-item-object-page <?php
								if(isset($_GET['p'])){
									checked_menu($_GET['p'],"trang-chu");
								}else{
									echo " current-menu-item page_item page-item-96 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor ";
								}
							?> menu-item-has-children menu-item-97">
								<a href="index.php?p=trang-chu">Trang Chủ</a>	
							</li>
							<li id="menu-item-122" class="menu-item menu-item-type-post_type menu-item-object-page <?php
								if(isset($_GET['p'])){
									checked_menu($_GET['p'],"dich-vu");
								}
							?>  menu-item-has-children menu-item-122">
									<a href="index.php?p=dich-vu">Dịch Vụ</a>
							</li>
							<li id="menu-item-122" class="menu-item menu-item-type-post_type menu-item-object-page <?php
								if(isset($_GET['p'])){
									checked_menu($_GET['p'],"tour-khach-le");
									checked_menu($_GET['p'],"tour-hanh-huong");
									checked_menu($_GET['p'],"chi-tiet-tour");
								}
							?> menu-item-has-children menu-item-122">
									<a href="#">Tours</a>
								<ul class="sub-menu">

									<?php
										$data_menu=load_menu_loai_tour($conn);
										foreach ($data_menu as  $value) {
									 ?>
									<li id="menu-item-274" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-274"><a href="index.php?p=tour&idlt=<?php echo $value['id'] ?>"><?php echo $value['name'] ?></a>
									</li>
									<?php } ?>
									<!-- <li id="menu-item-275" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-275"><a href="index.php?p=tour-hanh-huong">Hành Hương</a>
									</li> -->
									
								</ul>
							</li>

							<li id="menu-item-121" class="menu-item menu-item-type-post_type menu-item-object-page <?php
								if(isset($_GET['p'])){
									checked_menu($_GET['p'],"thu-vien");
								}
							?> menu-item-121"><a href="index.php?p=thu-vien">Thư Viện</a>
							</li>
							<li id="menu-item-100" class="menu-item menu-item-type-post_type menu-item-object-page <?php
								if(isset($_GET['p'])){
									checked_menu($_GET['p'],"lien-he");
								}
							?> menu-item-100"><a href="index.php?p=lien-he">Liên Hệ</a>
							</li>
						</ul>
					</div>				
				</nav>
				<div class="clear"></div>
				<div class="select-menu select-field">
					<select>
						<option value="index.php?p=trang-chu">Trang Chủ</option>
						<option value="index.php?p=dich-vu">Dịch Vụ </option>
						<option value="index.php?p=tour">Tour
						</option>
						<option value="index.php?p=thu-vien">Thư Viện</option>
						<option value="index.php?p=lien-he">Liên Hệ</option>
					</select><span>&nbsp;</span>
				</div>
			<!--/ select menu-->
			</div>
			<!-- supheader -->
			<?php 
				if (isset($_GET['p'])) {
				$p = $_GET['p'];
				switch ($p) {
					case 'trang-chu':
						include 'dh96_module/dashbroad/book_tour.php';
						break;
					case 'dich-vu':
						break;
					case 'lien-he':
						break;
					case 'chi-tiet-tour':
						break;
					case 'tour':
						break;
					case 'thu-vien':
						break;
					default:
						include 'dh96_module/dashbroad/book_tour.php';
						break;
				}
			}else{
				include 'dh96_module/dashbroad/book_tour.php';
			}
			 ?>
			<!-- subheader -->
			<div class="block-background header-background"></div>
		</header>
		<!-- header -->

		<?php 
			if (isset($_GET['p'])) {
				$p = $_GET['p'];
				switch ($p) {
					case 'trang-chu':
						include 'dh96_module/dashbroad/index.php';
						break;
					case 'dich-vu':
						include 'dh96_module/dich_vu/dichvu.php';
						break;
					case 'lien-he':
						include 'dh96_module/lien_he/contact.php';
						break;
					case 'chi-tiet-tour':
						include 'dh96_module/chi_tiet_tour/chi-tiet-tour.php';
						break;
					case 'tour':
						include 'dh96_module/loai_tour/tours.php';
						break;
					case 'thu-vien':
						include 'dh96_module/thuvien/thuvien.php';
						break;
					default:
						include 'dh96_module/dashbroad/index.php';
						break;
				}
			}else{
				include 'dh96_module/dashbroad/index.php';
			}

		 ?>


		<!-- content -->


		<footer class="container site-footer">		
			<div class="row">
				<div class="copyright">
					Gà Hùng &copy; 2017				
				</div>
					</div>
		</footer>
		<!-- footer -->
		<div class="substrate bottom-substrate">
			<img src="temp/images/site_bg.jpg" class="fullwidth" alt="" />		
		</div>
	</div>


	<!-- javarscip -->
	<script type='text/javascript' src='temp/js/jquery.ui.core.min.js'></script>
	<script type='text/javascript' src='temp/js/jquery.ui.widget.min.js'></script>
	<script type='text/javascript' src='temp/js/jquery.ui.mouse.min.js'></script>
	<script type='text/javascript' src='temp/js/jquery.ui.slider.min.js'></script>
	<script type='text/javascript' src='temp/js/jquery.ui.datepicker.min.js'></script>
</body>
</html>