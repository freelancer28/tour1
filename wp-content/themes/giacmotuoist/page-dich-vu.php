<?php get_header(); ?>

<section class="container site-content">
  <div class="row">
    <div class="tabs-container vertical-tabs">
      <div class="column threecol tabs">
        <ul>
          <?php foreach (getField('servicepage_booking_group_box') as $key => $value) { ?>
            <li><a><?php echo $value['servicepage_booking_group_title'] ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <div class="panes column ninecol last">
        <?php foreach (getField('servicepage_booking_group_box') as $key => $value) { ?>
          <div class="pane">
            <div class="sevencol column">
              <h3><?php echo $value['servicepage_booking_group_caption'] ?></h3>
              <p><strong><?php echo $value['servicepage_booking_group_description'] ?></strong></p>
              <a href="/lien-he/" target="_self" class="button medium primary">Liên Hệ</a>
            </div>
            <div class="fivecol column last">
              <img class="aligncenter" alt="" src="<?php echo $value['servicepage_booking_group_picture'] ?>" />
            </div>
            <div class="clear"></div>
          </div>
        <?php } ?>
      </div>
    </div>
    <div class="fivecol column">
      <div class="section-title">
        <h4>Ý kiến khách hàng</h4>
      </div>
      <div class="staff-block">
        <?php if ($queryCustomerReviewHome->have_posts()) { ?>
          <?php while ($queryCustomerReviewHome->have_posts()) { ?>
            <?php $queryCustomerReviewHome->the_post(); ?>
            <div class="fourcol column">
              <div class="featured-image">
                <img src="<?php echo getField('customerreviews_picture') ?>" alt="" />
              </div>
            </div>
            <div class="eightcol column last">
              <h5><?php the_title(); ?></h5>
              <p><?php echo getField('customerreviews_description') ?></p>
            </div>
            <div class="clear"></div>&nbsp;<br />
          <?php } ?>
          <?php wp_reset_postdata(); ?>
        <?php } ?>
        <div class="clear"></div>
      </div>
    </div>
    <div class="fourcol column">
      <?php if ($queryWhyChoose->have_posts()) { ?>
        <div class="section-title">
          <h4>Tại sao chọn chúng tôi</h4>
        </div>
        <?php while ($queryWhyChoose->have_posts()) { ?>
          <?php $queryWhyChoose->the_post(); ?>
          <div class="toggle">
            <div class="toggle-title"><?php the_title() ?></div>
            <div class="toggle-content"><?php echo getField('why_choose_description') ?></div>
          </div>
        <?php } ?>
        <?php wp_reset_postdata(); ?>
      <?php } ?>
    </div>
    <div class="threecol column last">
      <div class="section-title">
        <h4>Liên hệ</h4>
      </div>
      <div class="widget widget_text">			
        <div class="textwidget">
          <div class="featured-image">
            <a href="index.html">
              <h3><?php echo get_option('tnt_company_name'); ?></h3>
            </a>
            <div class="media">
              <div class="row1">
                <span class="icon" id="diachi"></span>
                <span class="content-diachi" ><?php echo get_option('tnt_company_address'); ?></span>
              </div>
              <div class="row1">
                <span class="icon" id="sdt"></span>
                <span  class="content-sdt"><?php echo get_option('tnt_company_phone1'); ?></span>
              </div>
              <div class="row1">
                <span class="icon" id="email"></span>
                <span  class="content"><?php echo get_option('tnt_company_email'); ?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>							
  </div>		
</section>

<!-- content -->

<?php get_footer(); ?>