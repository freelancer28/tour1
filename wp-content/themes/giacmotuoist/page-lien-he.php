<?php get_header(); ?>
<section class="container site-content">
  <div class="row">
    <div class="col"></div>
    <div class="col"></div>
  </div>
</section>
<div class="map-container align-top">
  <div class="map-canvas"><?php echo get_option('tnt_script_address'); ?></div>
</div>
<section class="container site-content">
  <div class="row">
    <div class="eightcol column">
      <div class="section-title">
        <h1>Liên hệ</h1>
      </div>
      <?php
      if ($_POST['contact'] == 'Gửi') {
        $admin_email = get_option('admin_email');
        $to = $admin_email;
        $subject = "Midway Contact";
        $message = sanitize_text_field(trim($_POST['full-name'])) . "\r\n";
        $message .= sanitize_text_field(trim($_POST['email-address'])) . "\r\n";
        $message .= sanitize_text_field(trim($_POST['message'])) . "\r\n";
        $result = wp_mail($to, $subject, $message);
        if ($result === true) {
          echo "<p class='message-success'>Chúng tôi đã gửi yêu cầu của bạn.</p>";
        }
      }
      ?>
      <form action="" method="POST" class="formatted-form">
        <p></p>
        <div class="message"></div>
        <div class="sixcol column ">
          <div class="field-container">
            <input type="text" id="full-name" name="full-name" value="" placeholder="Full Name"/>
          </div>
        </div>
        <div class="sixcol column last">
          <div class="clear"></div>
          <div class="field-container">
            <input type="text" id="email-address" name="email-address" value="" placeholder="Email Address"  />
          </div>
        </div>
        <div class="clear"></div>
        <div class="field-container">
          <textarea id="message" name="message" placeholder="Message" ></textarea>
        </div>
        <input type="submit" class="action" name="contact" value="Gửi" id="btnQuangCao">
      </form>
    </div>
    <div class="fourcol column last">
      <?php get_template_part('part/subscribe', 'home'); ?>
      <div class="widget widget_text">
        <div class="section-title">
          <h4>Chi tiết </h4>
        </div>			
        <div class="textwidget">
          <strong>Văn phòng:</strong> <?php echo get_option('tnt_company_address'); ?><br />
          <strong>Điện thoại:</strong> <?php echo get_option('tnt_company_phone1'); ?><br />
          <strong>Email:</strong> <?php echo get_option('tnt_company_email'); ?>
        </div>
      </div>
    </div>
    <div class="clear"></div>							
  </div>		
</section>
<!-- content -->

<?php get_footer(); ?>