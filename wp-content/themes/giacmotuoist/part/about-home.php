<section class="container site-content">
  <div class="row">
    <div class="eightcol column">
      <div class="fivecol column">
        <img class="alignnone size-medium wp-image-21 demo-image" title="image_1" src="<?php echo getField('homepage_aboutpicture'); ?>" alt="<?php echo getField('homepage_abouttitle'); ?>" />
      </div>
      <div class="sevencol column last">
        <div class="section-title">
          <h1><?php echo getField('homepage_abouttitle'); ?></h1>
        </div>
        <?php echo getField('homepage_aboutdescription'); ?>
      </div>
      <div class="clear"></div>
    </div>
    <?php if ($queryCustomerReviewHome->have_posts()) { ?>
      <div class="fourcol column last">
        <div class="content-slider testimonials-slider">
          <ul>
            <?php while ($queryCustomerReviewHome->have_posts()) { ?>
              <?php $queryCustomerReviewHome->the_post(); ?>
              <li>
                <article class="testimonial">
                  <div class="quote-text">
                    <div class="block-background"><?php echo getField('customerreviews_description') ?></div>
                  </div>
                  <h6 class="quote-author"><?php the_title(); ?></h6>
                </article>
              </li>
            <?php } ?>
            <?php wp_reset_postdata(); ?>
          </ul>
          <input type="hidden" class="slider-pause" value="0" /><input type="hidden" class="slider-speed" value="400" />
        </div>
      </div>
    <?php } ?>
    <div class="clear"></div>
  </div>
</section>