<?php if ($queryTourHome->have_posts()) { ?>
  <section class="container content-section">
    <div class="substrate section-substrate">
      <img src="<?php echo get_option('tnt_background'); ?>" class="fullwidth" alt="" />
    </div>
    <div class="row">
      <div class="items-grid">
        <?php $countTour = 1; ?>
        <?php while ($queryTourHome->have_posts()) { ?>
          <?php $queryTourHome->the_post(); ?>
          <?php $classNameTour = ''; ?>
          <?php if ($countTour % 4 == 0) { ?>
            <?php $classNameTour = 'last'; ?>
          <?php } ?>
          <div class="column threecol <?php echo $classNameTour; ?>">
            <div class="tour-thumb-container">
              <div class="tour-thumb">
                <a href="<?php the_permalink() ?>">
                  <img width="440" height="330" src="<?php echo getImgInList (getField('tours_album')); ?>" class="attachment-preview wp-post-image" alt="<?php echo the_title(); ?>" />
                </a>
                <div class="tour-caption">
                  <h5 class="tour-title">
                    <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                  </h5>
                  <div class="tour-meta">
                    <div class="tour-destination">
                      <div class="colored-icon icon-2"></div>
                      <a href="#" rel="tag"><?php echo getField('tours_destination') ?></a>
                    </div>
                    <div class="tour-duration"><?php echo getField('tours_time') ?> ngày</div>
                  </div>
                </div>			
              </div>
              <div class="block-background"></div>
            </div>
          </div>
          <?php $countTour += 1; ?>
        <?php } ?>
        <?php wp_reset_postdata(); ?>
      </div>
    </div>
  </section>
<?php } ?>