<?php if ($queryNewsHome->have_posts()) { ?>
  <div class="widget widget-twitter">
    <div class="section-title">
      <h4>Tin tức mới nhất</h4>
    </div>
    <div id="tweets">
      <ul>
        <?php while ($queryNewsHome->have_posts()) { ?>
          <?php $queryNewsHome->the_post(); ?>
          <li>
            <div class="colored-icon icon-4"></div>
            <div class="tweet">
              <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
              <div class="tweet-wrap"><?php echo getField('news_description') ?></div>
            </div>
          </li>
        <?php } ?>
        <?php wp_reset_postdata(); ?>
      </ul>
    </div>
  </div>
<?php } ?>