<?php
$termsCategoryTour = get_terms([
  'taxonomy' => 'category_tour',
  'hide_empty' => false
  ]);

$termsLocationTour = get_terms([
  'taxonomy' => 'location_tour',
  'hide_empty' => false
  ]);
?>

<div class="threecol column subheader-block">
  <div class="tour-search-form placeholder-form">
    <div class="form-title">
      <h4>Tìm kiếm chuyến đi</h4>
    </div>
    <form method="get" class="searchform" action="<?php echo home_url(''); ?>" >
      <div class="select-field">
        <span>Tất cả các địa điểm</span>
        <select name='s' id='s' class='postform' >
          <option value='all'>Tất cả các địa điểm</option>
          <?php foreach ($termsLocationTour as $key => $value) { ?>
            <option value='<?php echo $value->slug ?>'><?php echo $value->name ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="select-field">
        <span>Loại tours</span>
        <select name='cat' id='cat' class='postform' >
          <option value='all'>Loại tours </option>
          <?php foreach ($termsCategoryTour as $key => $value) { ?>
            <option value='<?php echo $value->slug ?>'><?php echo $value->name ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="field-container">
        <input type="text" name="date_dep" class="date-field" value="Ngày khởi hành" />
      </div>
      <div class="select-field">
        <span>Số ngày đi:</span>
        <select name='countday' id='count-day' class='postform' >
          <option value='0'>Số ngày đi</option>
          <option class="level-0" value="1"> 1 Ngày </option>
          <option class="level-0" value="2"> 2 Ngày </option>
          <option class="level-0" value="3"> 3 Ngày </option>
          <option class="level-0" value="4"> 4 Ngày </option>
          <option class="level-0" value="5"> 5 Ngày </option>
          <option class="level-0" value="6"> 6 Ngày </option>
        </select>
      </div>
      <div class="select-field">
        <span>Giá từ</span>
        <select name='money' id='money' class='postform' >
          <option value='0'>Giá từ </option>
          <option class="level-0" value="1"> dưới 5.000.000 VNĐ </option>
          <option class="level-0" value="2"> từ 5.000.000 VNĐ - 10.000.000 VNĐ </option>
          <option class="level-0" value="3"> từ 10.000.000 VNĐ - 20.000.000 VNĐ </option>
          <option class="level-0" value="4"> từ 20.000.000 VNĐ - 30.000.000 VNĐ </option>
          <option class="level-0" value="5"> từ 30.000.000 VNĐ - 50.000.000 VNĐ </option>
          <option class="level-0" value="6"> trên 50.000.000 VNĐ </option>
        </select>
      </div>
      <div class="form-button">
        <div class="button-container">
          <button type="submit" class="button" name="submit" value="Chọn tour" >Chọn tour</button>
        </div>
      </div>
    </form>
  </div>
  <!-- tour search form -->								
</div>