<div class="widget widget-subscribe">
  <div class="section-title">
    <h4>Bản tin</h4>
  </div>
  <p>Đăng ký bản tin của chúng tôi ngay bây giờ để cập nhật với những gì mới với Midway</p>
  <?php
  if ($_POST['subscribe'] == 'Gửi') {
    $to = trim($_POST['email']);
    $subject = "Thông tin mới từ Midway";
    $message = "Cảm ơn bạn đã đăng ký. Chúng tôi sẽ chia sẻ các tour bạn yêu thích.";
    $result = wp_mail($to, $subject, $message);
    if ($result === true) {
      echo "<p class='message-success'>Chúng tôi đã gửi mail cho bạn</p>";
    }
  }
  ?>
  <form action="" method="POST">
    <div class="message"></div>
    <div class="field-container">
      <input type="text" name="email" placeholder="Email Address" />
    </div>
    <input type="submit" class="action" name="subscribe" value="Gửi" id="btnQuangCao" />
  </form>
</div>