<aside class="column threecol last">
  <div class="widget widget_text">			
    <div class="textwidget">
      <div class="featured-image">
        <a href="index.html">
          <h3><?php echo get_option('tnt_company_name'); ?></h3>
        </a>
        <div class="media">
          <div class="row1">
            <span class="icon" id="diachi"></span>
            <span class="content-diachi"><?php echo get_option('tnt_company_address'); ?></span>
          </div>
          <div class="row1">
            <span class="icon" id="sdt"></span>
            <span class="content-sdt"><?php echo get_option('tnt_company_phone1'); ?></span>
          </div>
          <div class="row1">
            <span class="icon" id="email"></span>
            <span class="content"><?php echo get_option('tnt_company_email'); ?></span>
          </div>

        </div>
      </div>
    </div>
  </div>
  <?php if ($queryPartner->have_posts()) { ?>
    <div class="widget widget_text">
      <?php while ($queryPartner->have_posts()) { ?>
        <?php $queryPartner->the_post(); ?>
        <div class="textwidget">
          <div class="featured-image">
            <a href="<?php echo getField('partner_link') ?>">
              <img id="img_tour" src="<?php echo getField('partner_picture'); ?>" alt="">
            </a>
          </div>
          <a href="#booking-form" data-id="82" data-title="Samui Holidays" class="button small colorbox inline cboxElement"><span><?php echo getField('partner_text') ?></span></a>
        </div>
      <?php } ?>
      <?php wp_reset_postdata(); ?>
    </div>
  <?php } ?>
</aside>