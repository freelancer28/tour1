<?php
$args = array(
  'post_type' => 'slider',
  'post_status' => array('publish'),
  'orderby' => 'post_date',
  'order' => 'DESC',
  'posts_per_page' => 10,
);
$querySliderHome = new WP_Query($args);
?>
<?php if ($querySliderHome->have_posts()) { ?>
  <div class="ninecol column subheader-block last">
    <div class="main-slider-container content-slider-container">
      <div class="content-slider main-slider">
        <ul>
          <?php while ($querySliderHome->have_posts()) { ?>
            <?php $querySliderHome->the_post(); ?>
            <li>
              <div class="featured-image">
                <a href="<?php echo getField('sliders_link'); ?>">
                  <img width="824" height="370" src="<?php echo getField('sliders_picture'); ?>" class="attachment-large wp-post-image" alt="<?php the_title() ?>" />
                </a>
              </div>
            </li>
          <?php } ?>
          <?php wp_reset_postdata(); ?>
        </ul>
        <div class="arrow arrow-left content-slider-arrow"></div>
        <div class="arrow arrow-right content-slider-arrow"></div>
        <input type="hidden" class="slider-pause" value="2000" />
        <input type="hidden" class="slider-speed" value="50" />
        <input type="hidden" class="slider-effect" value="fade" />
      </div>
      <div class="block-background layer-1"></div>
      <div class="block-background layer-2"></div>
    </div>
  </div>
<?php } ?>