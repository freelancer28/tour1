<?php if ($queryGalleryHome->have_posts()) { ?>
  <div class="sixcol1 column">
    <div class="section-title">
      <h1>Thư Viện</h1>
    </div>
    <div class="items-grid">
      <?php $countGalleryHome = 1; ?>
      <?php while ($queryGalleryHome->have_posts()) { ?>
        <?php $queryGalleryHome->the_post(); ?>
        <?php $classGalleryHome = ''; ?>
        <?php
        if ($countGalleryHome % 3 == 0) {
          $classGalleryHome = 'last';
        }
        ?>
        <div class="column gallery-item fourcol <?php echo $classGalleryHome; ?>">
          <div class="featured-image">
            <a href="<?php echo getField('gallerys_picture'); ?>" class="colorbox " data-group="gallery-111" title="<?php the_title(); ?>">
              <img width="440" height="330" src="<?php echo getField('gallerys_picture'); ?>" class="attachment-preview wp-post-image" alt="<?php the_title(); ?>" />		
            </a>
            <a class="featured-image-caption hidden-caption">
              <h6><?php the_title(); ?></h6>
            </a>			
          </div>
          <?php foreach (getField('gallerys_album') as $key => $value) { ?>
            <div class="hidden">	
              <a class="colorbox" href="<?php echo $value; ?>" data-group="gallery-111"></a>
            </div>
          <?php } ?>
          <div class="block-background"></div>
        </div>
        <?php
        $countGalleryHome += 1;
      }
      ?>
      <?php wp_reset_postdata(); ?>
      <div class="clear"></div>
    </div>
  </div>
<?php } ?>