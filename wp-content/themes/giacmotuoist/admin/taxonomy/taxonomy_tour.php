<?php

function register_taxonomy_tour() {
  $labels = array(
    "name" => __("Category tours", "giacmotuoist"),
    "singular_name" => __("Category tour", "giacmotuoist"),
  );

  $args = array(
    "label" => __("Category tours", "giacmotuoist"),
    "labels" => $labels,
    "public" => true,
    "publicly_queryable" => true,
    "hierarchical" => true,
    "show_ui" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "query_var" => true,
    "rewrite" => array('slug' => 'category_tour', 'with_front' => true, 'hierarchical' => true,),
    "show_admin_column" => true,
    "show_in_rest" => true,
    "rest_base" => "category_tour",
    "rest_controller_class" => "WP_REST_Terms_Controller",
    "show_in_quick_edit" => true,
  );
  register_taxonomy("category_tour", array("tour"), $args);

  $labels = array(
    "name" => __("Location tour", "giacmotuoist"),
    "singular_name" => __("Location tour", "giacmotuoist"),
  );

  $args = array(
    "label" => __("Location tours", "giacmotuoist"),
    "labels" => $labels,
    "public" => true,
    "publicly_queryable" => true,
    "hierarchical" => true,
    "show_ui" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "query_var" => true,
    "rewrite" => array('slug' => 'location_tour', 'with_front' => true, 'hierarchical' => true,),
    "show_admin_column" => true,
    "show_in_rest" => true,
    "rest_base" => "location_tour",
    "rest_controller_class" => "WP_REST_Terms_Controller",
    "show_in_quick_edit" => true,
  );
  register_taxonomy("location_tour", array("tour"), $args);
}

add_action('init', 'register_taxonomy_tour');
?>