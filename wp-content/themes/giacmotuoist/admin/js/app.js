jQuery(function ($) {
  if ($('.set_custom_images').length > 0) {
    if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
      $(document).on('click', '.set_custom_images', function (e) {
        e.preventDefault();
        let buttonImage = $(this);
        let inputImage = buttonImage.prev();
        wp.media.editor.send.attachment = function (props, attachment) {
          inputImage.val(attachment.url);
        };
        wp.media.editor.open(buttonImage);
        return false;
      });
    }
  }
});