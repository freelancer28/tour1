<?php

function tnt_register_header_theme() {
  register_setting('tnt_header_theme', 'logo');
}

add_action('admin_init', 'tnt_register_header_theme');

function tnt_register_footer_theme() {
  register_setting('tnt_footer_theme', 'tnt_copyright');
}

add_action('admin_init', 'tnt_register_footer_theme');

function tnt_register_social_theme() {
  register_setting('tnt_social_theme', 'tnt_link_facebook');
  register_setting('tnt_social_theme', 'tnt_link_vimeo');
}

add_action('admin_init', 'tnt_register_social_theme');

function tnt_register_contact_theme() {
  register_setting('tnt_contact_theme', 'tnt_company_name');
  register_setting('tnt_contact_theme', 'tnt_company_address');
  register_setting('tnt_contact_theme', 'tnt_company_phone1');
  register_setting('tnt_contact_theme', 'tnt_company_email');
  register_setting('tnt_contact_theme', 'tnt_script_address');
}

add_action('admin_init', 'tnt_register_contact_theme');

function tnt_register_global_theme() {
  register_setting('tnt_global_theme', 'tnt_background');
  register_setting('tnt_global_theme', 'tnt_background1');
}

add_action('admin_init', 'tnt_register_global_theme');