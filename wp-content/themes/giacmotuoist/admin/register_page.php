<?php

function register_option_theme_page() {
  add_menu_page('Options Theme', 'Options Theme', 'manage_options', 'option_header_page_menu', 'option_header_page', '', null);
  add_submenu_page('option_header_page_menu', 'Headers', 'Headers', 'manage_options', 'option_header_page_menu', 'option_header_page');
  add_submenu_page('option_header_page_menu', 'Footers', 'Footers', 'manage_options', 'option_footer_page_menu', 'option_footer_page');
  add_submenu_page('option_header_page_menu', 'Contacts', 'Contacts', 'manage_options', 'option_contact_page_menu', 'option_contact_page');
  add_submenu_page('option_header_page_menu', 'Social', 'Social', 'manage_options', 'option_social_page_menu', 'option_social_page');
  add_submenu_page('option_header_page_menu', 'Globals', 'Globals', 'manage_options', 'option_global_page_menu', 'option_global_page');
}

add_action('admin_menu', 'register_option_theme_page');

function option_header_page() {
  global $title;
  require_once( TEMPLATEPATH . '/admin/templates/view_headers.php' );
}

function option_footer_page() {
  global $title;
  require_once( TEMPLATEPATH . '/admin/templates/view_footers.php' );
}

function option_social_page() {
  global $title;
  require_once( TEMPLATEPATH . '/admin/templates/view_social.php' );
}

function option_contact_page() {
  global $title;
  require_once( TEMPLATEPATH . '/admin/templates/view_contact.php' );
}

function option_global_page() {
  global $title;
  require_once( TEMPLATEPATH . '/admin/templates/view_global.php' );
}
