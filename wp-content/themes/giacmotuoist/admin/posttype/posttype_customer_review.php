<?php

function register_posttype_customer_review() {
  $labels = array(
    "name" => __("Customer reviews", "giacmotuoist"),
    "singular_name" => __("Customer review", "giacmotuoist"),
  );

  $args = array(
    "label" => __("Customer reviews", "giacmotuoist"),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => false,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array("slug" => "customer_review", "with_front" => true),
    "query_var" => true,
    "supports" => array("title"),
  );

  register_post_type("customer_review", $args);
}

add_action('init', 'register_posttype_customer_review');
?>