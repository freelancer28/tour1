<?php

function register_posttype_tour() {
  $labels = array(
    "name" => __("Tours", "giacmotuoist"),
    "singular_name" => __("Tour", "giacmotuoist"),
  );

  $args = array(
    "label" => __("Tours", "giacmotuoist"),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => true,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => true,
    "rewrite" => array("slug" => "tour", "with_front" => true),
    "query_var" => true,
    "supports" => array("title", "editor"),
    "taxonomies" => array("category_tour"),
  );

  register_post_type("tour", $args);
}

add_action('init', 'register_posttype_tour');
?>