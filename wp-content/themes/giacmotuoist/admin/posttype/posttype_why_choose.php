<?php

function register_posttype_why_choose() {
  $labels = array(
    "name" => __("Why choose", "giacmotuoist"),
    "singular_name" => __("Gallery", "giacmotuoist"),
  );

  $args = array(
    "label" => __("Why chooses", "giacmotuoist"),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => true,
    "rewrite" => array("slug" => "why_choose", "with_front" => true),
    "query_var" => true,
    "supports" => array("title"),
  );

  register_post_type("why_choose", $args);
}

add_action('init', 'register_posttype_why_choose');
?>