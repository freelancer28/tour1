<?php

function register_posttype_partner() {
  $labels = array(
    "name" => __("Partner", "giacmotuoist"),
    "singular_name" => __("Partner", "giacmotuoist"),
  );

  $args = array(
    "label" => __("Partner", "giacmotuoist"),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => true,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => true,
    "rewrite" => array("slug" => "partner", "with_front" => false),
    "query_var" => true,
    "supports" => array("title")
  );

  register_post_type("partner", $args);
}

add_action('init', 'register_posttype_partner');
?>