<?php $tnt_background = get_option('tnt_background'); ?>
<?php $tnt_background1 = get_option('tnt_background1'); ?>
<div class="container">
  <div class="row">
    <h2><?php echo $title; ?></h2>
    <div class="wrap wrap-admin">
      <form id="tnt_register_header_theme" method="post" action="options.php">
        <?php settings_fields('tnt_global_theme'); ?>
        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="tnt_background"><?php echo __('Background image'); ?></label>
            <div class="form-image">
              <input id="tnt_background" name="tnt_background" type="text" class="form-control" value="<?php echo $tnt_background; ?>">
              <button class="set_custom_images button">Set Image</button>
            </div>
          </div>
          <?php
          if ($tnt_background != ''):
            ?>
            <img src="<?php echo $tnt_background; ?>"/>
            <?php
          endif;
          ?>
        </div>
        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="tnt_background1"><?php echo __('Background image1'); ?></label>
            <div class="form-image">
              <input id="tnt_background1" name="tnt_background1" type="text" class="form-control" value="<?php echo $tnt_background1; ?>">
              <button class="set_custom_images button">Set Image</button>
            </div>
          </div>
          <?php
          if ($tnt_background1 != ''):
            ?>
            <img src="<?php echo $tnt_background1; ?>"/>
            <?php
          endif;
          ?>
        </div>
        <?php submit_button(); ?>
      </form>
    </div>
  </div>
</div>