<?php
$tnt_copyright = get_option('tnt_copyright');
?>
<div class="container">
  <div class="row">
    <h2><?php echo $title; ?></h2>
    <div class="wrap wrap-admin">
      <form id="form" method="post" action="options.php">
        <?php settings_fields('tnt_footer_theme'); ?>

        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="tnt_copyright"><?php echo __('Copyright'); ?></label>
            <textarea class="form-control" id="tnt_copyright" name="tnt_copyright"><?php echo $tnt_copyright ?></textarea>
          </div>
        </div>

        <?php submit_button(); ?>
      </form>
    </div>
  </div>
</div>
<script>
  jQuery(function ($) {
    $('#form').validate({
      onkeyup: function (e) {
        $(e).valid();
      },
      rules: {
        'tnt_copyright': {
          required: true
        }
      }
    });
  });
</script>