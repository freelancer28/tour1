<?php
$tnt_link_facebook = get_option('tnt_link_facebook');
$tnt_link_vimeo = get_option('tnt_link_vimeo');
?>
<div class="container">
  <div class="row">
    <h2><?php echo $title; ?></h2>
    <div class="wrap wrap-admin">
      <form id="form" method="post" action="options.php">
        <?php settings_fields('tnt_social_theme'); ?>

        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="tnt_link_facebook"><?php echo __('Link facebook'); ?></label>
            <input class="form-control" id="tnt_link_facebook" name="tnt_link_facebook" value="<?php echo $tnt_link_facebook; ?>">
          </div>
        </div>

        <?php submit_button(); ?>
      </form>
    </div>
  </div>
</div>