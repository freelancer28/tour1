<?php $form_logo = get_option('logo'); ?>
<div class="container">
  <div class="row">
    <h2><?php echo $title; ?></h2>
    <div class="wrap wrap-admin">
      <form id="tnt_register_header_theme" method="post" action="options.php">
        <?php settings_fields('tnt_header_theme'); ?>
        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="logo"><?php echo __('Logo'); ?></label>
            <div class="form-image">
              <input id="logo" name="logo" type="text" class="form-control" value="<?php echo $form_logo; ?>">
              <button class="set_custom_images button">Set Image</button>
            </div>
          </div>
          <?php
          if ($form_logo != ''):
            ?>
            <img src="<?php echo $form_logo; ?>"/>
            <?php
          endif;
          ?>
        </div>
        <?php submit_button(); ?>
      </form>
    </div>
  </div>
</div>
<script>
  jQuery(function ($) {
    $('#tnt_register_header_theme').validate({
      onkeyup: function (e) {
        $(e).valid();
      },
      rules: {
        'logo': {
          required: true
        }
      }
    });
  });
</script>