<?php $ads1 = get_option('ads1'); ?>
<?php $ads1_link = get_option('ads1_link'); ?>
<div class="container">
  <div class="row">
    <h2><?php echo $title; ?></h2>
    <div class="wrap wrap-admin">
      <form id="tnt_register_header_theme" method="post" action="options.php">
        <?php settings_fields('tnt_ads_theme'); ?>
        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="ads1"><?php echo __('Ads1'); ?></label>
            <div class="form-image">
              <input id="ads1" name="ads1" type="text" class="form-control" value="<?php echo $ads1; ?>">
              <button class="set_custom_images button">Set Image</button>
            </div>
          </div>
          <?php
          if ($ads1 != ''):
            ?>
            <img src="<?php echo $ads1; ?>"/>
            <?php
          endif;
          ?>
        </div>
        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="ads1_link"><?php echo __('Link1'); ?></label>
            <textarea class="form-control" id="ads1_link" name="ads1_link"><?php echo $ads1_link ?></textarea>
          </div>
        </div>
        <?php submit_button(); ?>
      </form>
    </div>
  </div>
</div>
<script>
  jQuery(function ($) {
    $('#tnt_register_header_theme').validate({
      onkeyup: function (e) {
        $(e).valid();
      },
      rules: {
        'logo': {
          required: true
        }
      }
    });
  });
</script>