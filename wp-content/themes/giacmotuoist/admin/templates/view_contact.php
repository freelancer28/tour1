<?php
$tnt_company_name = get_option('tnt_company_name');
$tnt_company_address = get_option('tnt_company_address');
$tnt_company_phone1 = get_option('tnt_company_phone1');
$tnt_company_email = get_option('tnt_company_email');
$tnt_script_address = get_option('tnt_script_address');
?>
<div class="container">
  <div class="row">
    <h2><?php echo $title; ?></h2>
    <div class="wrap wrap-admin">
      <form id="form" method="post" action="options.php">
        <?php settings_fields('tnt_contact_theme'); ?>

        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="tnt_company_name"><?php echo __('Company name'); ?></label>
            <input type="text" class="form-control" id="tnt_company_name" name="tnt_company_name" value="<?php echo $tnt_company_name; ?>" />
          </div>
        </div>
        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="tnt_company_address"><?php echo __('Company address'); ?></label>
            <input type="text" class="form-control" id="tnt_company_address" name="tnt_company_address" value="<?php echo $tnt_company_address; ?>" />
          </div>
        </div>
        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="tnt_company_phone1"><?php echo __('Company phone'); ?></label>
            <input type="text" class="form-control" id="tnt_company_phone1" name="tnt_company_phone1" value="<?php echo $tnt_company_phone1; ?>" />
          </div>
        </div>
        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="tnt_company_email"><?php echo __('Company email'); ?></label>
            <input type="text" class="form-control" id="tnt_company_email" name="tnt_company_email" value="<?php echo $tnt_company_email; ?>" />
          </div>
        </div>
        <div class="row-admin">
          <div class="input-field col s12">
            <label class="label-admin" for="tnt_script_address"><?php echo __('Iframe google map'); ?></label>
            <textarea class="form-control" id="tnt_script_address" name="tnt_script_address"><?php echo $tnt_script_address; ?></textarea>
          </div>
        </div>

        <?php submit_button(); ?>
      </form>
    </div>
  </div>
</div>
<script>
  jQuery(function ($) {
    $('#form').validate({
      onkeyup: function (e) {
        $(e).valid();
      },
      rules: {
        'tnt_company_name': {
          required: true
        },
        'tnt_company_address': {
          required: true
        },
        'tnt_company_phone1': {
          required: true
        },
        'tnt_company_email': {
          required: true,
          email: true
        }
      }
    });
  });
</script>