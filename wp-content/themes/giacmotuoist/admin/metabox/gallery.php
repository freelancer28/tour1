<?php

function gallerys_metabox() {
  $prefix = 'gallerys_';
  $meta_box = new_cmb2_box(array(
    'id' => $prefix . 'info',
    'title' => __('Thông tin', 'cmb2'),
    'object_types' => array('gallery'), // Post type
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));

  $meta_box->add_field(array(
    'name' => __('Picture', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'picture',
    'type' => 'file'
  ));

  $meta_box->add_field(array(
    'name' => __('Album', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'album',
    'type' => 'file_list'
  ));
  
}

add_action('cmb2_init', 'gallerys_metabox');
?>