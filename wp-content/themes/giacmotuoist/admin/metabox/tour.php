<?php

function tours_metabox() {
  $prefix = 'tours_';
  $meta_box = new_cmb2_box(array(
    'id' => $prefix . 'info',
    'title' => __('Info', 'cmb2'),
    'object_types' => array('tour'), // Post type
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));
  
  $meta_box->add_field(array(
    'name' => __('Description', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'description',
    'type' => 'textarea',
    'attributes' => array(
      'class' => 'form-control',
    ),
  ));

  $meta_box->add_field(array(
    'name' => __('Destination', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'destination',
    'type' => 'text',
    'attributes' => array(
      'class' => 'form-control',
    ),
  ));

  $meta_box->add_field(array(
    'name' => __('Time', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'time',
    'type' => 'text',
    'attributes' => array(
      'class' => 'form-control',
    ),
  ));

  $meta_box->add_field(array(
    'name' => __('Price', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'price',
    'type' => 'text',
    'attributes' => array(
      'class' => 'form-control',
    ),
  ));
  
  $meta_box->add_field(array(
    'name' => __('Tour price includes', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'tour_price_includes',
    'type' => 'textarea',
    'attributes' => array(
      'class' => 'form-control',
    ),
  ));
  
  $meta_box->add_field(array(
    'name' => __('Price does not include', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'price_does_not_includes',
    'type' => 'textarea',
    'attributes' => array(
      'class' => 'form-control',
    ),
  ));
  
  $meta_box->add_field(array(
    'name' => __('Album', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'album',
    'type' => 'file_list'
  ));
  
}

add_action('cmb2_init', 'tours_metabox');
?>