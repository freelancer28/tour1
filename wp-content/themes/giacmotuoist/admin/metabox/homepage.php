<?php

function homepage_metabox()
{
  $prefix = 'homepage_about';

  $meta_box = new_cmb2_box(array(
    'id' => $prefix,
    'title' => __('About', 'cmb2'),
    'object_types' => array('page'), // Post type
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true, // Show field names on the left
    'show_on' => array('key' => 'id', 'value' => array(2)),
  ));

  $meta_box->add_field(array(
    'name' => __('Picture', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'picture',
    'type' => 'file'
  ));

  $meta_box->add_field(array(
    'name' => __('Title', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'title',
    'type' => 'text'
  ));

  $meta_box->add_field(array(
    'name' => __('Description', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'description',
    'type' => 'textarea'
  ));

}

add_action('cmb2_init', 'homepage_metabox');
?>