<?php

function news_metabox() {
  $prefix = 'news_';
  $meta_box = new_cmb2_box(array(
    'id' => $prefix . 'info',
    'title' => __('Info', 'cmb2'),
    'object_types' => array('post'), // Post type
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));

  $meta_box->add_field(array(
    'name' => __('Description', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'description',
    'type' => 'textarea',
    'attributes' => array(
      'class' => 'form-control',
    ),
  ));
  
}

add_action('cmb2_init', 'news_metabox');
?>