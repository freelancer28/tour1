<?php

function partner_metabox() {
  $prefix = 'partner_';
  $meta_box = new_cmb2_box(array(
    'id' => $prefix . 'info',
    'title' => __('Info', 'cmb2'),
    'object_types' => array('partner'), // Post type
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));

  $meta_box->add_field(array(
    'name' => __('Picture', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'picture',
    'type' => 'file'
  ));

  $meta_box->add_field(array(
    'name' => __('Link', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'link',
    'type' => 'text_url'
  ));
  
   $meta_box->add_field(array(
    'name' => __('Text', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'text',
    'type' => 'text'
  ));
  
}

add_action('cmb2_init', 'partner_metabox');
?>