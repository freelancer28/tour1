<?php

function sliders_metabox() {
  $prefix = 'sliders_';
  $meta_box = new_cmb2_box(array(
    'id' => $prefix . 'info',
    'title' => __('Thông tin', 'cmb2'),
    'object_types' => array('slider'), // Post type
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));

  $meta_box->add_field(array(
    'name' => __('Link', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'link',
    'type' => 'text_url',
    'attributes' => array(
      'class' => 'form-control',
    ),
  ));

  $meta_box->add_field(array(
    'name' => __('Picture', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'picture',
    'type' => 'file'
  ));
}

add_action('cmb2_init', 'sliders_metabox');
?>