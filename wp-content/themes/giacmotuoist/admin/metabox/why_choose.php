<?php

function why_choose_metabox() {
  $prefix = 'why_choose_';
  $meta_box = new_cmb2_box(array(
    'id' => $prefix . 'info',
    'title' => __('Info', 'cmb2'),
    'object_types' => array('why_choose'), // Post type
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));

  $meta_box->add_field(array(
    'name' => __('Description', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'description',
    'type' => 'textarea'
  ));
}

add_action('cmb2_init', 'why_choose_metabox');
?>