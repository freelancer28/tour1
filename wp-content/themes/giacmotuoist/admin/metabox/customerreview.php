<?php

function customerreviews_metabox() {
  $prefix = 'customerreviews_';
  $meta_box = new_cmb2_box(array(
    'id' => $prefix . 'info',
    'title' => __('Info', 'cmb2'),
    'object_types' => array('customer_review'), // Post type
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));

  $meta_box->add_field(array(
    'name' => __('Description', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'description',
    'type' => 'textarea'
  ));

  $meta_box->add_field(array(
    'name' => __('Picture', 'cmb2'),
    'desc' => __('', 'cmb2'),
    'id' => $prefix . 'picture',
    'type' => 'file'
  ));
}

add_action('cmb2_init', 'customerreviews_metabox');
?>