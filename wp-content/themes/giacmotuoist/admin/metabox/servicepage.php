<?php

function servicepage_metabox() {
  $prefixBokking = 'servicepage_booking_';
  $prefixBokkingGroup = 'servicepage_booking_group_';

  $meta_box = new_cmb2_box(array(
    'id' => $prefixBokking . 'box',
    'title' => __('Booking', 'cmb2'),
    'object_types' => array('page'), // Post type
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true, // Show field names on the left
    'show_on' => array('key' => 'id', 'value' => array(53)),
  ));

  $meta_box_group = $meta_box->add_field(array(
    'id' => $prefixBokkingGroup . 'box',
    'type' => 'group',
    'description' => __('', 'cmb2'),
    // 'repeatable'  => false, // use false if you want non-repeatable group
    'options' => array(
      'group_title' => __('Entry {#}', 'cmb2'), // since version 1.1.4, {#} gets replaced by row number
      'add_button' => __('Add Another Entry', 'cmb2'),
      'remove_button' => __('Remove Entry', 'cmb2'),
      'sortable' => true, // beta
    // 'closed'     => true, // true to have the groups closed by default
    )
  ));
  
  $meta_box->add_group_field($meta_box_group, array(
    'name' => 'Title',
    'description' => 'Title',
    'id' => $prefixBokkingGroup . 'title',
    'type' => 'text'
  ));

  $meta_box->add_group_field($meta_box_group, array(
    'name' => 'Caption',
    'description' => 'Caption',
    'id' => $prefixBokkingGroup . 'caption',
    'type' => 'text'
  ));

  $meta_box->add_group_field($meta_box_group, array(
    'name' => 'Description',
    'description' => 'Description',
    'id' => $prefixBokkingGroup . 'description',
    'type' => 'textarea'
  ));

  $meta_box->add_group_field($meta_box_group, array(
    'name' => 'Picture',
    'description' => 'Picture',
    'id' => $prefixBokkingGroup . 'picture',
    'type' => 'file'
  ));
}

add_action('cmb2_init', 'servicepage_metabox');
?>