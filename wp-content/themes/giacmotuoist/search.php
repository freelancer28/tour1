<?php get_header(); ?>

<?php
$address =$_GET['s'];
$location = $_GET['cat'];
$countday = $_GET['countday'];
$money = $_GET['money'];
$start = 0;
$end = 0;
if ($money == 1) {
  $end = 5000000;
}elseif ($money == 2) {
  $start = 5000001;
  $end = 10000000;
}elseif ($money == 3) {
  $start = 10000001;
  $end = 20000000;
}elseif ($money == 4) {
  $start = 20000001;
  $end = 30000000;
}elseif ($money == 5) {
  $start = 30000001;
  $end = 50000000;
}else{
  $start = 50000001;
  $end = 100000000000000;
}
$tax_query = [];
if ($address != 'all' && $location != 'all') {
  $tax_query = array(
        'relation' => 'AND',
        array(
            'taxonomy' => 'category_tour',
            'field'    => 'slug',
            'terms'    => $location,
        ),
        array(
            'taxonomy' => 'location_tour',
            'field'    => 'slug',
            'terms'    => $address,
        ),
    );
}
if($address == 'all' && $location != 'all'){
  $tax_query = array(
        array(
            'taxonomy' => 'category_tour',
            'field'    => 'slug',
            'terms'    => $location,
        ),
    );
}

if($address != 'all' && $location == 'all'){
  $tax_query = array(
        array(
            'taxonomy' => 'location_tour',
            'field'    => 'slug',
            'terms'    => $address,
        ),
    );
}
$args = [];
if (count($tax_query) == 0) {
  $args = array(
      'post_type'             => 'tour',
      'posts_per_page'        => -1,
  );
  
}else{
  $args = array(
      'post_type'             => 'tour',
      'posts_per_page'        => -1,
      'tax_query' => $tax_query ,
  );
}

$the_query = new WP_Query( $args );
?>
<section class="container site-content">
  <div class="row">
    <div class="column ninecol">
      <?php
      if (isset($_POST['btnbookking']) && $_POST['btnbookking'] == 'Gửi') {
        $admin_email = get_option('admin_email');
        $to = $admin_email;
        $subject = "Đặt tour [Midway]";
        $message = sanitize_text_field(trim($_POST['full-name'])) . "\r\n";
        $message .= sanitize_text_field(trim($_POST['email-address'])) . "\r\n";
        $message .= 'Ngày khởi hành: ' . sanitize_text_field(trim($_POST['departure-date'])) . "\r\n";
        $message .= 'Ngày đến: ' . sanitize_text_field(trim($_POST['arrival-date'])) . "\r\n";
        $message .= 'Số người: ' . sanitize_text_field(trim($_POST['adults-number'])) . "\r\n";
        $message .= 'Số trẻ em: ' . sanitize_text_field(trim($_POST['kids-number'])) . "\r\n";
        $result = wp_mail($to, $subject, $message);
        if ($result === true) {
          echo "<p class='message-success'>Chúng tôi đã gửi yêu cầu của bạn.</p>";
        }
      }
      ?>
      <?php $i=0 ?>
      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>	
        <?php if ($countday != 0 && $money != 0): ?>
          <?php if (getField('tours_time') == $countday && ($start <= (int)getField('tours_price') && (int)getField('tours_price') <= $end ) ): ?>
            <div class="items-list clearfix">
              <div class="full-tour clearfix">
                <div class="fivecol column">    
                  <div class="content-slider-container tour-slider-container">
                    <div class="featured-image">
                      <a href="<?php the_permalink() ?>">
                        <img width="550" height="413" src="<?php echo getImgInList(getField('tours_album')) ?>" class="attachment-extended wp-post-image" alt="image_12">
                      </a>
                    </div>
                    <div class="block-background layer-2"></div>
                  </div>    
                </div>
                <div class="sevencol column last">
                  <div class="section-title">
                    <h1><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h1>
                  </div>
                  <ul class="tour-meta">
                    <li>
                      <div class="colored-icon icon-2"></div>
                      <strong>Địa điểm:</strong>
                      <a><?php echo getField('tours_destination') ?></a>
                    </li>
                    <li>
                      <div class="colored-icon icon-1"><span></span></div>
                      <strong>Thời gian:</strong> <?php echo getField('tours_time') ?>
                    </li>
                    <li><div class="colored-icon icon-3">
                        <span></span></div><strong>Số tiền:</strong> <?php echo number_format(getField('tours_price'), 0, ',', '.') ?> VNĐ
                    </li>
                  </ul>
                  <p><?php echo getField('tours_description') ?></p>
                  <footer class="tour-footer">
                    <a href="#booking-form" data-id="82" data-title="Samui Holidays" class="button small colorbox inline cboxElement"><span>Đặt Tour</span></a>
                    <!-- <a href="#question-form" data-id="82" data-title="Samui Holidays" class="button grey small colorbox inline"><span>Ask a Question</span></a> -->
                    <a href="<?php the_permalink() ?>" type="button" class="button">Chi tiết tour</a>
                  </footer>
                </div>

              </div>
              <div class="clear"></div> 
            </div>
            <?php $i++; ?>
          <?php endif ?>
        <?php elseif($countday == 0 && $money != 0) : ?>
          <?php if ($start <= (int)getField('tours_price') && (int)getField('tours_price') <= $end ): ?>
            <div class="items-list clearfix">
              <div class="full-tour clearfix">
                <div class="fivecol column">    
                  <div class="content-slider-container tour-slider-container">
                    <div class="featured-image">
                      <a href="<?php the_permalink() ?>">
                        <img width="550" height="413" src="<?php echo getImgInList(getField('tours_album')) ?>" class="attachment-extended wp-post-image" alt="image_12">
                      </a>
                    </div>
                    <div class="block-background layer-2"></div>
                  </div>    
                </div>
                <div class="sevencol column last">
                  <div class="section-title">
                    <h1><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h1>
                  </div>
                  <ul class="tour-meta">
                    <li>
                      <div class="colored-icon icon-2"></div>
                      <strong>Địa điểm:</strong>
                      <a><?php echo getField('tours_destination') ?></a>
                    </li>
                    <li>
                      <div class="colored-icon icon-1"><span></span></div>
                      <strong>Thời gian:</strong> <?php echo getField('tours_time') ?>
                    </li>
                    <li><div class="colored-icon icon-3">
                        <span></span></div><strong>Số tiền:</strong> <?php echo number_format(getField('tours_price'), 0, ',', '.') ?> VNĐ
                    </li>
                  </ul>
                  <p><?php echo getField('tours_description') ?></p>
                  <footer class="tour-footer">
                    <a href="#booking-form" data-id="82" data-title="Samui Holidays" class="button small colorbox inline cboxElement"><span>Đặt Tour</span></a>
                    <!-- <a href="#question-form" data-id="82" data-title="Samui Holidays" class="button grey small colorbox inline"><span>Ask a Question</span></a> -->
                    <a href="<?php the_permalink() ?>" type="button" class="button">Chi tiết tour</a>
                  </footer>
                </div>

              </div>
              <div class="clear"></div> 
            </div>
            <?php $i++; ?>
          <?php endif ?>
        <?php elseif($countday != 0 && $money == 0) : ?>
          <?php if (getField('tours_time') == $countday): ?>
            <div class="items-list clearfix">
              <div class="full-tour clearfix">
                <div class="fivecol column">    
                  <div class="content-slider-container tour-slider-container">
                    <div class="featured-image">
                      <a href="<?php the_permalink() ?>">
                        <img width="550" height="413" src="<?php echo getImgInList(getField('tours_album')) ?>" class="attachment-extended wp-post-image" alt="image_12">
                      </a>
                    </div>
                    <div class="block-background layer-2"></div>
                  </div>    
                </div>
                <div class="sevencol column last">
                  <div class="section-title">
                    <h1><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h1>
                  </div>
                  <ul class="tour-meta">
                    <li>
                      <div class="colored-icon icon-2"></div>
                      <strong>Địa điểm:</strong>
                      <a><?php echo getField('tours_destination') ?></a>
                    </li>
                    <li>
                      <div class="colored-icon icon-1"><span></span></div>
                      <strong>Thời gian:</strong> <?php echo getField('tours_time') ?>
                    </li>
                    <li><div class="colored-icon icon-3">
                        <span></span></div><strong>Số tiền:</strong> <?php echo number_format(getField('tours_price'), 0, ',', '.') ?> VNĐ
                    </li>
                  </ul>
                  <p><?php echo getField('tours_description') ?></p>
                  <footer class="tour-footer">
                    <a href="#booking-form" data-id="82" data-title="Samui Holidays" class="button small colorbox inline cboxElement"><span>Đặt Tour</span></a>
                    <!-- <a href="#question-form" data-id="82" data-title="Samui Holidays" class="button grey small colorbox inline"><span>Ask a Question</span></a> -->
                    <a href="<?php the_permalink() ?>" type="button" class="button">Chi tiết tour</a>
                  </footer>
                </div>

              </div>
              <div class="clear"></div> 
            </div>
            <?php $i++; ?>
          <?php endif ?>
        <?php else: ?>
            <div class="items-list clearfix">
              <div class="full-tour clearfix">
                <div class="fivecol column">    
                  <div class="content-slider-container tour-slider-container">
                    <div class="featured-image">
                      <a href="<?php the_permalink() ?>">
                        <img width="550" height="413" src="<?php echo getImgInList(getField('tours_album')) ?>" class="attachment-extended wp-post-image" alt="image_12">
                      </a>
                    </div>
                    <div class="block-background layer-2"></div>
                  </div>    
                </div>
                <div class="sevencol column last">
                  <div class="section-title">
                    <h1><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h1>
                  </div>
                  <ul class="tour-meta">
                    <li>
                      <div class="colored-icon icon-2"></div>
                      <strong>Địa điểm:</strong>
                      <a><?php echo getField('tours_destination') ?></a>
                    </li>
                    <li>
                      <div class="colored-icon icon-1"><span></span></div>
                      <strong>Thời gian:</strong> <?php echo getField('tours_time') ?>
                    </li>
                    <li><div class="colored-icon icon-3">
                        <span></span></div><strong>Số tiền:</strong> <?php echo number_format(getField('tours_price'), 0, ',', '.') ?> VNĐ
                    </li>
                  </ul>
                  <p><?php echo getField('tours_description') ?></p>
                  <footer class="tour-footer">
                    <a href="#booking-form" data-id="82" data-title="Samui Holidays" class="button small colorbox inline cboxElement"><span>Đặt Tour</span></a>
                    <!-- <a href="#question-form" data-id="82" data-title="Samui Holidays" class="button grey small colorbox inline"><span>Ask a Question</span></a> -->
                    <a href="<?php the_permalink() ?>" type="button" class="button">Chi tiết tour</a>
                  </footer>
                </div>

              </div>
              <div class="clear"></div> 
            </div>
            <?php $i++; ?>
        <?php endif ?>
          
      <?php endwhile; ?>
      <?php if ($i == 0): ?>
        <p>No result</p>
      <?php endif ?>
      <nav class="pagination">
        <?php
        $big = 999999999;
        echo paginate_links(array(
          'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
          'format' => '?paged=%#%',
          'current' => max(1, get_query_var('paged')),
          'total' => $query->max_num_pages,
          'mid_size' => 1,
          'prev_text' => __('«'),
          'next_text' => __('»'),
          'type' => 'list'
        ));
        ?>
      </nav>
      <?php wp_reset_postdata(); ?>
    </div> 
    <?php set_query_var('queryPartner', $queryPartner); ?>
    <?php get_template_part('part/sidebar', 'tour'); ?>
    <div class="hidden">
      <div class="booking-form popup-form" id="booking-form">
        <div class="section-title popup-title"><h4></h4></div>
        <form method="POST" class="formatted-form">		
          <div class="sixcol column ">
            <div class="field-container">
              <input type="text" id="full-name" name="full-name" value="" placeholder="Họ và tên">
            </div>
          </div>
          <div class="sixcol column last">
            <div class="clear"></div>
            <div class="field-container">
              <input type="text" id="email-address" name="email-address" value="" placeholder="Email">
            </div>
          </div>
          <div class="sixcol column ">
            <div class="field-container">
              <input type="text" id="departure-date" name="departure-date" value="" class="date-field hasDatepicker" placeholder="Ngày khởi hành">
            </div>
          </div>
          <div class="sixcol column last">
            <div class="clear"></div>
            <div class="field-container"><input type="text" id="arrival-date" name="arrival-date" value="" class="date-field hasDatepicker" placeholder="Ngày đến">
            </div>
          </div>
          <div class="sixcol column ">
            <div class="field-container">
              <input type="text" id="adults-number" name="adults-number" value="" placeholder="Số người">
            </div>
          </div>
          <div class="sixcol column last">
            <div class="clear"></div>
            <div class="field-container"><input type="text" id="kids-number" name="kids-number" value="" placeholder="Số trẻ em"></div>
          </div>
          <input type="hidden" name="id" value="" class="popup-id">
          <input type="hidden" name="slug" value="booking">
          <input type="hidden" class="action" value="themex_form_submit">
          <input type="submit" name="btnbookking" value="Gửi">
        </form>
      </div>
      <!-- booking form -->
    </div>							
  </div>		
</section>

<?php get_footer(); ?>