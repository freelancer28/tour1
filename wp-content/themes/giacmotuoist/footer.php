<footer class="container site-footer">		
  <div class="row">
    <div class="copyright"><?php echo get_option('tnt_copyright'); ?></div>
  </div>
</footer>
<!-- footer -->
<div class="substrate bottom-substrate">
  <img src="<?php echo get_option('tnt_background1'); ?>" class="fullwidth" alt="" />		
</div>
</div>
<?php wp_footer(); ?>
<!-- javarscip -->
<script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.ui.core.min.js'></script>
<script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.ui.widget.min.js'></script>
<script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.ui.mouse.min.js'></script>
<script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.ui.slider.min.js'></script>
<script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.ui.datepicker.min.js'></script>
<script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/func.js'></script>
</body>
</html>