<?php get_header(); ?>

<?php get_template_part('part/banner', 'page'); ?>

<div class="layout-full">
  <div class="inner">
    <?php get_template_part('part/content', 'page'); ?>
  </div>
</div>

<?php get_footer(); ?>