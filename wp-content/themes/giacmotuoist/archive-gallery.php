<?php get_header(); ?>
<section class="container site-content">
  <div class="row">
    <?php if (have_posts()) : ?>
      <div class="items-grid">
        <?php $countGallery = 1; ?>
        <?php while (have_posts()) : the_post(); ?>
          <?php $classGallery = ''; ?>
          <?php
          if ($countGallery % 4 == 0) {
            $classGallery = 'last';
          }
          ?>
          <div class="column gallery-item threecol <?php echo $classGallery; ?>">
            <div class="featured-image">
              <a href="<?php echo getField('gallerys_picture'); ?>" class="colorbox " data-group="gallery-111" title="<?php the_title(); ?>">
                <img width="440" height="330" src="<?php echo getField('gallerys_picture'); ?>" class="attachment-preview wp-post-image" alt="<?php the_title(); ?>" />		
              </a>
              <a class="featured-image-caption hidden-caption">
                <h6><?php the_title(); ?></h6>
              </a>			
            </div>
            <?php foreach (getField('gallerys_album') as $key => $value) { ?>
              <div class="hidden">	
                <a class="colorbox" href="<?php echo $value; ?>" data-group="gallery-111"></a>
              </div>
            <?php } ?>
            <div class="block-background"></div>
          </div>
          <?php
          if ($classGallery == 'last') {
            echo '<div class="clear"></div>';
          }
          ?>
          <?php $countGallery += 1; ?>
        <?php endwhile; ?>
        <div class="clear"></div>
        <?php if (paginate_links() != '') { ?>
          <nav class="pagination">
            <?php
            global $wp_query;
            $big = 999999999;
            echo paginate_links(array(
              'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
              'format' => '?paged=%#%',
              'prev_text' => __('«'),
              'next_text' => __('»'),
              'current' => max(1, get_query_var('paged')),
              'total' => $wp_query->max_num_pages,
              'type' => 'list'
            ));
            ?>
          </nav>
        <?php } ?>
        <?php wp_reset_postdata(); ?>
        <br><br>
      </div>
    <?php else : ?>
      <!-- show 404 error here -->
    <?php endif; ?>
  </div>		
</section>

<!-- content -->

<?php get_footer(); ?>