<?php

define('DOMAIN_URL', get_template_directory_uri());

load_theme_textdomain('tour');

add_theme_support('title-tag');
add_theme_support('post-thumbnails');

function hideAdminBar() {
  return false;
}

add_filter('show_admin_bar', 'hideAdminBar');

if (function_exists('wp_nav_menu')) {

  function wp_my_menus() {
    register_nav_menus(array(
      'main' => __('Main Navigation', 'tour')
    ));
  }

  add_action('init', 'wp_my_menus');
}

function adminStyle() {
  if (is_admin()) {
    wp_enqueue_style('admin-styles', get_template_directory_uri() . '/admin/css/admin.css');
    wp_enqueue_script('admin-script', get_template_directory_uri() . '/admin/js/jquery.validate.js');
    wp_enqueue_script('admin-app-script', get_template_directory_uri() . '/admin/js/app.js');
    wp_enqueue_media();
  }
}

add_action('admin_enqueue_scripts', 'adminStyle');

require_once( TEMPLATEPATH . '/admin/posttype/posttype_slider.php' );
require_once( TEMPLATEPATH . '/admin/posttype/posttype_gallery.php' );
require_once( TEMPLATEPATH . '/admin/posttype/posttype_customer_review.php' );
require_once( TEMPLATEPATH . '/admin/posttype/posttype_tour.php' );
require_once( TEMPLATEPATH . '/admin/posttype/posttype_why_choose.php' );
require_once( TEMPLATEPATH . '/admin/posttype/posttype_adssidebar.php' );

require_once( TEMPLATEPATH . '/admin/taxonomy/taxonomy_tour.php' );

require_once( TEMPLATEPATH . '/admin/register_option.php' );
require_once( TEMPLATEPATH . '/admin/register_page.php' );

require_once( TEMPLATEPATH . '/admin/metabox/slider.php' );
require_once( TEMPLATEPATH . '/admin/metabox/gallery.php' );
require_once( TEMPLATEPATH . '/admin/metabox/homepage.php' );
require_once( TEMPLATEPATH . '/admin/metabox/customerreview.php' );
require_once( TEMPLATEPATH . '/admin/metabox/tour.php' );
require_once( TEMPLATEPATH . '/admin/metabox/news.php' );
require_once( TEMPLATEPATH . '/admin/metabox/servicepage.php' );
require_once( TEMPLATEPATH . '/admin/metabox/why_choose.php' );
require_once( TEMPLATEPATH . '/admin/metabox/partner.php' );

function getField($slug) {
  return get_post_meta(get_the_ID(), $slug, true);
}

function getImgInList($data) {
  foreach ($data as $key => $value) {
    return $value;
  }
}

$argsGalleryHome = array(
  'post_type' => 'gallery',
  'post_status' => array('publish'),
  'orderby' => 'post_date',
  'order' => 'DESC',
  'posts_per_page' => 9,
);
$queryGalleryHome = new WP_Query($argsGalleryHome);

$argsCustomerReviewHome = array(
  'post_type' => 'customer_review',
  'post_status' => array('publish'),
  'orderby' => 'post_date',
  'order' => 'DESC',
  'posts_per_page' => 2,
);
$queryCustomerReviewHome = new WP_Query($argsCustomerReviewHome);

$argsTourHome = array(
  'post_type' => 'tour',
  'post_status' => array('publish'),
  'orderby' => 'post_date',
  'order' => 'DESC',
  'posts_per_page' => 8,
);
$queryTourHome = new WP_Query($argsTourHome);

$argsNewsHome = array(
  'post_type' => 'post',
  'post_status' => array('publish'),
  'orderby' => 'post_date',
  'order' => 'DESC',
  'posts_per_page' => 4,
);
$queryNewsHome = new WP_Query($argsNewsHome);

$argsWhyChoose = array(
  'post_type' => 'why_choose',
  'post_status' => array('publish'),
  'orderby' => 'post_date',
  'order' => 'DESC',
  'posts_per_page' => 6,
);
$queryWhyChoose = new WP_Query($argsWhyChoose);

$argsPartner = array(
  'post_type' => 'partner',
  'post_status' => array('publish'),
  'orderby' => 'post_date',
  'order' => 'DESC',
  'posts_per_page' => 8,
);
$queryPartner = new WP_Query($argsPartner);