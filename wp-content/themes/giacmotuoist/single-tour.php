<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <section class="container site-content">
      <div class="row">
        <div class="full-tour clearfix">
          <?php
          if (isset($_POST['question']) && $_POST['question'] == 'Gửi') {
            $admin_email = get_option('admin_email');
            $to = $admin_email;
            $subject = "Đặt câu hỏi [Midway]";
            $message = sanitize_text_field(trim($_POST['full-name'])) . "\r\n";
            $message .= sanitize_text_field(trim($_POST['email-address'])) . "\r\n";
            $message .= sanitize_text_field(trim($_POST['message'])) . "\r\n";
            $result = wp_mail($to, $subject, $message);
            if ($result === true) {
              echo "<p class='message-success'>Chúng tôi đã gửi yêu cầu của bạn.</p>";
            }
          }
          if (isset($_POST['bookking']) && $_POST['bookking'] == 'Gửi') {
            $admin_email = get_option('admin_email');
            $to = $admin_email;
            $subject = "Đặt tour [Midway]";
            $message = sanitize_text_field(trim($_POST['full-name'])) . "\r\n";
            $message .= sanitize_text_field(trim($_POST['email-address'])) . "\r\n";
            $message .= 'Ngày khởi hành: '.sanitize_text_field(trim($_POST['departure-date'])) . "\r\n";
            $message .= 'Số người: '.sanitize_text_field(trim($_POST['adults-number'])) . "\r\n";
            $message .= 'Số trẻ em: '.sanitize_text_field(trim($_POST['kids-number'])) . "\r\n";
            $result = wp_mail($to, $subject, $message);
            if ($result === true) {
              echo "<p class='message-success'>Chúng tôi đã gửi yêu cầu của bạn.</p>";
            }
          }
          ?>
          <div class="sixcol column">
            <div class="content-slider-container tour-slider-container">
              <div class="content-slider tour-slider">
                <ul>
                  <?php foreach (getField('tours_album') as $key => $value) { ?>
                    <li><img src="<?php echo $value; ?>" alt="" /></li>
                  <?php } ?>
                </ul>
                <div class="arrow arrow-left content-slider-arrow"></div>
                <div class="arrow arrow-right content-slider-arrow"></div>
                <input type="hidden" class="slider-speed" value="400" />
                <input type="hidden" class="slider-pause" value="0" />
              </div>
              <div class="block-background layer-1"></div>
              <div class="block-background layer-2"></div>
            </div>		
          </div>
          <div class="fix sixcol column last fix">
            <div class="section-title">
              <h1><?php the_title() ?></h1>
            </div>
            <ul class="tour-meta">
              <li>
                <div class="colored-icon icon-2"></div>
                <strong>Điểm đến:</strong>
                <a href="" rel="tag"><?php echo getField('tours_destination') ?></a>
              </li>
              <li>
                <div class="colored-icon icon-1"><span></span></div>
                <strong>Thời gian:  </strong> <?php echo getField('tours_time') ?>
              </li>
              <li>
                <div class="colored-icon icon-3"><span></span></div>
                <strong>Giá:  </strong><?php echo number_format(getField('tours_price'), 0, ',', '.') ?> VNĐ
              </li>
            </ul>
            <div class="context-tour"><?php the_content(); ?></div>
            <footer class="tour-footer">
              <a href="#booking-form" data-id="82" data-title="Samui Holidays" class="button small colorbox inline"><span>Đặt Tour</span></a>
              <a href="#question-form" data-id="82" data-title="Samui Holidays" class="button grey small colorbox inline"><span>Đặt câu hỏi</span></a>
            </footer>
          </div>
        </div>
        <div class="hidden">
          <div class="booking-form popup-form" id="booking-form">
            <div class="section-title popup-title"><h4></h4></div>
            <form action="" method="POST" class="formatted-form">
              <div class="sixcol column ">
                <div class="field-container">
                  <input type="text" id="full-name" name="full-name" value="" placeholder="Họ tên"  />
                </div>
              </div>
              <div class="sixcol column last">
                <div class="clear"></div>
                <div class="field-container">
                  <input type="text" id="email-address" name="email-address" value="" placeholder="Địa chỉ email"  />
                </div>
              </div>
              <div class="sixcol column ">
                <div class="field-container">
                  <input type="text" id="departure-date" name="departure-date" value="" class="date-field" placeholder="Ngày khởi hành"  />
                </div>
              </div>
              <div class="sixcol column ">
                <div class="field-container">
                  <input type="text" id="adults-number" name="adults-number" value="" placeholder="Số người"  />
                </div>
              </div>
              <div class="sixcol column last">
                <div class="clear"></div>
                <div class="field-container">
                  <input type="text" id="kids-number" name="kids-number" value="" placeholder="Số trẻ em"  />
                </div>
              </div>			
              <input type="hidden" name="id" value="" class="popup-id" />
              <input type="hidden" name="slug" value="booking" />
              <input type="hidden" class="action" value="themex_form_submit" />
              <input type="submit" name="bookking" value="Gửi">
            </form>
          </div>
          <!-- booking form -->
          <div class="question-form popup-form" id="question-form">
            <div class="section-title popup-title"><h4></h4></div>
            <form method="post" class="formatted-form">
              <div class="sixcol column ">
                <div class="field-container">
                  <input type="text" id="full-name" name="full-name" value="" placeholder="Họ và tên"  />
                </div>
              </div>
              <div class="sixcol column last">
                <div class="clear"></div>
                <div class="field-container">
                  <input type="text" id="email-address" name="email-address" value="" placeholder="Địa chỉ email"  />
                </div>
              </div>
              <div class="clear"></div>
              <div class="field-container">
                <textarea id="message" name="message" placeholder="Câu hỏi?" ></textarea>
              </div>			
              <input type="hidden" name="id" value="" class="popup-id" />
              <input type="hidden" name="slug" value="question" />
              <input type="hidden" class="action" value="themex_form_submit" />
              <input type="submit" name="question" value="Gửi" />
            </form>
          </div>
        </div>
        <div class="sixcol column">
          <div class="tour-itinerary">
          </div>
        </div>
        <div class="sixcol column last">
          <ul class="title-right">
            <li>
              <strong>Giá tour bao gồm:</strong> 
              <div class="danh-sach-con">
                <p><?php echo getField('tours_tour_price_includes') ?></p>
              </div>
            </li>
          </ul>
          <ul class="title-right">
            <li>
              <strong>Giá tour không bao gồm:</strong> 
              <div class="danh-sach-con">
                <p><?php echo getField('tours_price_does_not_includes') ?></p>
              </div>
            </li>
          </ul>
        </div>
        <div class="clear"></div>
        <div class="commnet">
          <?php echo do_shortcode('[wpdevart_facebook_comment]'); ?>
        </div>
      </div>	
    </section>
    <!-- content -->
    <?php
  endwhile;
endif;
?>

<?php get_footer(); ?>