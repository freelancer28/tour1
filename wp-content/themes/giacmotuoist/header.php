<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
    <meta name="format-detection" content="telephone=no">
    <?php wp_head(); ?>

    <!--CSS  -->
    <link rel='stylesheet' id='colorbox-css' href='<?php echo DOMAIN_URL; ?>/assets/js/colorbox/colorbox.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-ui-datepicker-css' href='<?php echo DOMAIN_URL; ?>/assets/framework/assets/css/datepicker.css' type='text/css' media='all' />
    <link rel='stylesheet' id='general-css' href='<?php echo DOMAIN_URL; ?>/assets/css/style.css' type='text/css' media='all' />
    <!-- End css -->

    <!-- javarcrip -->
    <script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.js'></script>
    <script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/comment-reply.min.js'></script>
    <script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.hoverIntent.min.js'></script>
    <script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.ui.touchPunch.js'></script>
    <script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/colorbox/jquery.colorbox.min.js'></script>
    <script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.placeholder.min.js'></script>
    <script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.themexSlider.js'></script>
    <script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/jquery.textPattern.js'></script>
    <!-- End javarcrip -->

    <script type='text/javascript' src='<?php echo DOMAIN_URL; ?>/assets/js/general.js'></script>
    <link rel="shortcut icon" href="<?php echo DOMAIN_URL; ?>/assets/images/favicon.ico" />

  </head>
  <body <?php body_class(); ?>>
    <div class="container site-container">
      <header class="container site-header">
        <div class="substrate top-substrate">
          <img src="<?php echo get_option('tnt_background1'); ?>" class="fullwidth" alt="" />
        </div>
        <!-- background -->
        <div class="row supheader">
          <div class="logo">
            <a href="/" rel="home">
              <img src="<?php echo get_option('logo'); ?>" alt="Midway" />
            </a>
          </div>
          <!-- logo -->
          <div class="social-links">
            <a class="facebook" href="<?php echo get_option('tnt_link_facebook'); ?>" target="_blank" title="Facebook"></a>
            <a class="openmenu"><span>Menu</span></a>
          </div>
          <!-- social links -->
          <nav class="header-menu">
            <div class="menu">
              <a class="btnCloseMenu">Close</a>
              <?php
              wp_nav_menu(
                array(
                  'theme_location' => 'main',
                  'container' => 'false',
                  'menu_id' => 'menu-main-menu',
                  'menu_class' => 'menu'
                )
              );
              ?>
            </div>				
          </nav>
          <div class="clear"></div>
        </div>
        <?php if (is_front_page()) { ?>
          <!-- supheader -->
          <div class="row subheader">
            <?php get_template_part('part/searchtour', 'home'); ?>
            <?php get_template_part('part/slider', 'home'); ?>
          </div>
          <!-- subheader -->
        <?php } ?>
        <div class="block-background header-background"></div>
      </header>
      <!-- header -->

