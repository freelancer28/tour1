<?php get_header(); ?>

<?php set_query_var('queryCustomerReviewHome', $queryCustomerReviewHome); ?>
<?php get_template_part('part/about', 'home'); ?>

<?php set_query_var('queryTourHome', $queryTourHome); ?>
<?php get_template_part('part/tour', 'home'); ?>

<section class="container site-content">
  <div class="row">
    <?php set_query_var('queryGalleryHome', $queryGalleryHome); ?>
    <?php get_template_part('part/gallery', 'home'); ?>
    <div class="threecol column last">
      <?php get_template_part('part/subscribe', 'home'); ?>
      <?php set_query_var('queryNewsHome', $queryNewsHome); ?>
      <?php get_template_part('part/news', 'home'); ?>
    </div>
    <div class="clear"></div>							
  </div>		
</section>
<!-- content -->
<?php get_footer(); ?>