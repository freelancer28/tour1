<?php get_header(); ?>
<div class="boxMinHeight">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <section class="container site-content">
      <div class="row">
        <div class="full-tour clearfix">	
          <div class="fix column last fix">
            <div class="section-title">
              <h1><?php the_title() ?></h1>
            </div>
            <div class="context-tour"><?php the_content() ?></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>	
    </section>
    <?php
  endwhile;
endif;
?>
</div>
<?php get_footer(); ?>