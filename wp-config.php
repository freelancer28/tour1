<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'giacmotuoist');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'dV9pKXR47aw3');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'a.!T{mbX*jos/N[g7$I)NS^=%`B;zS_zx@@vJFR`mHX/]+A$XrT*X.Q4C{_5?AKz');
define('SECURE_AUTH_KEY',  'toV5Z_~Dy%.-E=jiRDx7UCZ hyAmJZz.Sy3*xmjGC!0?FYL hDGQWT4QVJWsCi y');
define('LOGGED_IN_KEY',    'kzV)M(x`_!*c@c}9&o#%5i7+!o[-rHs^qP]XE`RybVgZn/jKb8O>Oic Fi;TH%DH');
define('NONCE_KEY',        '2QjeOa+rBgWFkK?]Ta;kncOr ?DseZRgwRW+J6(3BWKE5J2N~s-*^[1qDG7~1^z7');
define('AUTH_SALT',        'lyI>2(KHP`I?Vy?yGgz<nW3w9&,C0u?pr(s8;d}k:=b,wW T[gcO:bvxlkE=wErA');
define('SECURE_AUTH_SALT', ']VN`vIZmD.(Wr7d|2a#xP]Or;`4;iQs^l$%HU|@<-B9F]9SP2F8L9E!mh^2N=q[`');
define('LOGGED_IN_SALT',   '/E+_gw&eMV,Dq{+_r+HZ>Bm0C1A9.2wv.o0IqKl23SF+KLq5B;=Ks$M.Qq>iV@CK');
define('NONCE_SALT',       '@s$Gcd~YUBs&FBaHmVpy/keYEv/eU7(|X=`Al.91v;NPM$Zr7IF?.w*]u?1{fxm)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gmtst_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', FALSE);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** Disable FTP */
define('FS_METHOD','direct');